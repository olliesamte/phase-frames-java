#!/bin/bash

#runs the script named in the first argument as if it was running on all Pis.

#pis="101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118"
pis="3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20"
echo "Sending file "$1" to the following Pis, destination folder: "$2"."

for pi in $pis
do
echo "Copying to "${pi}"."
#run the copy command
scp -r $1 pi@192.168.1.${pi}:$2
echo "...done."
done