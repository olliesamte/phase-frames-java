#!/bin/bash

#runs the script named in the first argument as if it was running on all Pis.

pis=`less pi_hostnames.txt`
echo "Sending file "$1" to the following Pis, destination folder: "$2"."

for pi in $pis
do
echo "Copying to "${pi}".local."
#run the copy command
scp -r /Users/ollie/git/phase-frames-java pi@${pi}.local:/home/pi/git/
ssh pi@${pi}.local "sudo reboot"
echo "...done."
done