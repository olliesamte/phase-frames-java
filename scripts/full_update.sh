#!/bin/bash

#runs the script named in the first argument as if it was running on all Pis.

./runOnPis.sh local_scripts/git_zap.sh
./copyToPis.sh ${HOME}/git/phase-frames-java /home/pi/git/
./runOnPis.sh local_scripts/correct_config_files.sh
./runOnPis.sh local_scripts/rebootPi.sh