#!/bin/bash

STPSTRING=#rtp{sdp=rtsp://:$1/}
raspivid -w $2 -h $3 -fps $4 -t 0 -n -o - | cvlc -vvv stream:///dev/stdin --sout "${STPSTRING}" :demux=h264
