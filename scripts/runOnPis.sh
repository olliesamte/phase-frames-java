#!/bin/bash

#runs the script named in the first argument as if it was running on all Pis.

pis=`less pi_hostnames.txt`
echo "Contacting the following Pis with script "$1": "$pis"."

for pi in $pis
do
echo "ssh-ing into "${pi}".local."
#login and update git
ssh pi@${pi}.local 'bash -s' < $1 &
#sleep 1s
echo "...done."
done
