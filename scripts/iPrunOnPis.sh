#!/bin/bash

#runs the script named in the first argument as if it was running on all Pis.

#pis="101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118"
pis="3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20"


echo "Contacting the following Pis with script "$1": "$pis"."




for i in $pis
do
echo "ssh-ing into "192.168.1.$i"."
#login and update git
ssh -o ConnectTimeout=5 pi@192.168.1.$i 'bash -s' < $1
echo "...done."
done