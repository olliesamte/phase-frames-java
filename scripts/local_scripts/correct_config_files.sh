#!/bin/bash

#currently we are running as a user, which means we are using bashrc and xinitrc_user, but not xinitrc_global and not rc.local.

#global files
sudo cp /home/pi/git/phase-frames-java/pi_setup/inittab         /etc/
sudo cp /home/pi/git/phase-frames-java/pi_setup/rc.local        /etc/
sudo cp /home/pi/git/phase-frames-java/pi_setup/xinitrc_global  /etc/X11/xinit/xinitrc
sudo cp /home/pi/git/phase-frames-java/pi_setup/boot_config     /boot/config.txt
sudo cp /home/pi/git/phase-frames-java/pi_setup/cmdline.txt	/boot/cmdline.txt

#local files
cp /home/pi/git/phase-frames-java/pi_setup/bashrc               /home/pi/.bashrc
cp /home/pi/git/phase-frames-java/pi_setup/run.sh               /home/pi/autorun/
cp /home/pi/git/phase-frames-java/pi_setup/net_test.sh          /home/pi/autorun/
cp /home/pi/git/phase-frames-java/pi_setup/xinitrc_user         /home/pi/.xinitrc
chmod 700 autorun/run.sh
