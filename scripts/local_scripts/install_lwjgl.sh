#!/bin/bash

#we seem to have proxy setup issues when running this remotely, but this does not seem to fix it.
export http_proxy="http://www-cache.sydney.edu.au:8080"
export https_proxy="https://www-cache.sydney.edu.au:8080"
export ftp_proxy="ftp://www-cache.sydney.edu.au:8080"
echo `env`

sudo apt-get -y install liblwjgl-java