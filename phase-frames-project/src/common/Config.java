package common;

import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Hashtable;
import java.util.Random;

public abstract class Config {
	
	//general settings
	public static final Random RNG = new Random();
	
	//runtime settings
	public static final double FPS = 15;
	public static final double MS_PER_FRAME = (1000. / FPS);
	public static final double FRAMES_PER_MS = 1. / MS_PER_FRAME;

	//the UDP buffers used for send and receive
	public static final int EXTRA_BUFFER_DATA_SIZE = 12;
	public static final int MAX_BUFFER_SIZE = 65000 - EXTRA_BUFFER_DATA_SIZE;
	
	//print stream (client side - write to file)
	public static PrintStream stderr;
	public static PrintStream stdout;
	
	//actual screen dimensions (these are initialised in the static code below)
	public static int SCREEN_WIDTH;
	public static int SCREEN_HEIGHT;
	
	//movie settings
	public final static int MOVIE_WIDTH = 640;
	public final static int MOVIE_HEIGHT = 320;
	public final static int MOVIE_COLOUR_FORMAT = BufferedImage.TYPE_INT_RGB;	
	public final static String MOVIE_COMPRESSION_FORMAT = "jpeg";
	public final static float MOVIE_COMPRESSION_QUALITY = 1f;
	public final static double CROSSFADE_FRACTION = 0.6f;

	//playback time (frames) for incoming camera streams on the server	
	public static final int CAMERA_PLAY_DURATION_FRAMES = (int)(10 * FPS);
	public static final int CAMERA_SHOT_INTERVAL_FRAMES = 1;
	public static final int TIME_RESTING_FRAMES = 20;
	
	//buffer size (frames) for devices
	public static final int DEVICE_BUFFER_SIZE_FRAMES = 250;
	
	//delay time cycle period (frames)
	public static final int DELAY_TIME_CYCLE_PERIOD = 400;	
	
	//max delay time
	public static final double MAX_DELAY_FRAMES = FPS * 1.5;	
	
	//movies
	public static final String MEDIA_DIR = "/Users/ollie/Movies/phase-frames-movies";
	public static final String[] MOVIE_FILENAMES = new String[] {
//		"film.mp4",
		"PF_02.mov", 
		"PF_03.mov",
		"PF_04.mov",
		"PF_05.mov",
		"PF_06.mov",
		"PF_07.mov",
		"PF_08.mov",
		"PF_09.mov",
		"PF_10.mov",
		"PF_11.mov",
		"PF_12.mov",
	};
	
	//Networking config
	public static final String MULTICAST_ADDRESS = "224.1.1.1";
	//muticast video stream (server to clients)
	public static final int MULTICAST_STREAM_PORT = 9000;
	//timing ping (server to clients)
	public static final int MULTICAST_PING_PORT = 9001;
	// control (server to clients)
	public static final int MULTICAST_CONTROL_PORT_CPP = 9002;
	public static final int MULTICAST_CONTROL_PORT_JAVA = 9003;
	//camera stream (client to server), each camera streams on a different port starting with this #
	public static final int CAMERA_STREAM_START_PORT = 10000;
	//server name
	public static final String SERVER_HOSTNAME = "woof.local";
	
	//timing pinger
	public static final int PING_INTERVAL = 5000;
	   
	//hostnames
	public final static String[] HOSTNAMES = {
			"b827eb1958ea",
			"b827eb0aa031",
			"b827eb3e923f",
			"b827eb5b3a47",
			"b827eb19eb64",
			"b827ebf4c035",
			"b827eb658d29",
			"b827ebd40add",
			"b827ebd3c42f",
			"b827ebdf8039",
			"b827ebc93d8d",
			"b827eb9d001a",
			"b827eb78be42",
			"b827eb155ae2",
			"b827eb9eb223",
			"b827ebb8ec53",
			"b827ebc6f4ae",
			"b827ebd77d1c",
	};
	
	//table of device indices, maps hostname to index
	public static final Hashtable<String, Integer> DEVICES = new Hashtable<String, Integer>();
	
	//global init code
	static {
		setOutputToTerminal();
		//set screen size from screen, but be aware that this won't work outside of an X environment.
		try {
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			SCREEN_WIDTH = (int) screenSize.getWidth();
			SCREEN_HEIGHT = (int) screenSize.getHeight();
		} catch(HeadlessException e) {
			Config.stderr.println("We are running headless. Hope that's OK.");
		}
		//create the table of indices, hard wired
		for(int i = 0; i < HOSTNAMES.length; i++) {
			DEVICES.put(HOSTNAMES[i], i);
		}
	}
	
	//dynamically get numClients based on known devices list
	public static int numClients() {
		return DEVICES.size();
	}

	//output setting code
	public static void setOutputToTerminal() {
		stderr = System.err;
		stdout = System.out;
	}
	
	//output setting code
	public static void setOutputToFile(String dir) {
		try {
			stderr = new PrintStream(new File(dir + "/stderr"));
			stdout = new PrintStream(new File(dir + "/stdout"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
}
