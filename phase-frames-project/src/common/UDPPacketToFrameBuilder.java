package common;
import java.util.HashMap;

public class UDPPacketToFrameBuilder {
	
	//
	public static final boolean verbose = false;
	
	public int frameNumber = -1;
	public int targetPackets = -1;
	HashMap<Integer, byte[]> packets = new HashMap<Integer, byte[]>();
	HashMap<Integer, Integer> packetSizes = new HashMap<Integer, Integer>();
	
	public UDPPacketToFrameBuilder(int _frameNumber, int _targetPackets) {
		frameNumber = _frameNumber;
		targetPackets = _targetPackets;
	}
	
	public void AddPacket(byte[] packet, int packetNumber, int packetSize) {
		if(verbose) Config.stdout.println("Frame : " + frameNumber + " : packet number : " + packetNumber + " : packet length : " + packet.length + " : packetsize : " + packetSize);
		packets.put(packetNumber, packet);
		packetSizes.put(packetNumber, packetSize);
	}
	
	public boolean IsBufferReady() {
		return packets.size() == targetPackets;
	}
	
	public byte[] ConstructFrame() {
		int framesize = 0;
		for(int size: packetSizes.values()) {
			framesize += size - Config.EXTRA_BUFFER_DATA_SIZE;
		}
		byte[] frame = new byte[framesize];
		if(verbose) Config.stdout.println("Constructing frame of size : " + framesize);
		try {
			for(int i: packets.keySet()) {
				byte[] packet = packets.get(i);
				int packetLength = packetSizes.get(i);
				if(verbose) Config.stdout.println("Adding packet : " + i + " : to frame : " + frameNumber + " : packetlength : " + packetLength);
				for(int x = 0; x < packetLength - Config.EXTRA_BUFFER_DATA_SIZE; x++) {
					frame[i * Config.MAX_BUFFER_SIZE + x] = packet[x + Config.EXTRA_BUFFER_DATA_SIZE];
				}				
			}
		} catch (Exception e) {
			e.printStackTrace(Config.stderr);
		}
		return frame;
	}
}
