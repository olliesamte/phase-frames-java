package common;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import uk.co.caprica.vlcj.player.MediaPlayerFactory;
import uk.co.caprica.vlcj.player.direct.BufferFormat;
import uk.co.caprica.vlcj.player.direct.BufferFormatCallback;
import uk.co.caprica.vlcj.player.direct.DirectMediaPlayer;
import uk.co.caprica.vlcj.player.direct.RenderCallbackAdapter;
import uk.co.caprica.vlcj.player.direct.format.RV32BufferFormat;

import com.sun.jna.NativeLibrary;

public class MovieConverter {
	
	String outputPath;
	
	public static final boolean DRAW = true;	//NOT WORKING, DON'T SET FALSE

    private final int width = Config.MOVIE_WIDTH;
    private final int height = Config.MOVIE_HEIGHT;

    /**
     * Image to render the video frame data.
     */
    private final BufferedImage image;
    private final MediaPlayerFactory factory;
    private final DirectMediaPlayer mediaPlayer;
    private ImagePane imagePane;
    private JFrame frame;
    private boolean finished = false;
    private boolean nudged = false;
    boolean converting = false;
    
    private ArrayList<BufferedImage> data = new ArrayList<BufferedImage>();
    
    public static void main(String[] args) throws InterruptedException, InvocationTargetException {
    	NativeLibrary.addSearchPath("vlc", "/Applications/VLC.app/Contents/MacOS/lib/");
    	
    	for(String filename : Config.MOVIE_FILENAMES) {
    		System.out.println("Converting " + filename);
	        MovieConverter m = new MovieConverter(Config.MEDIA_DIR + "/original/" + filename, Config.MEDIA_DIR + "/raw/" + filename);
	        // Application will not exit since the UI thread is running
	        System.out.println("Created movie converter, now waiting");
	        while(!m.finished()) {
	        	//wait
	        	System.out.println("Waiting");
	        	m.nudge();
	        	Thread.sleep(1000);
	        }
	        System.out.println("Exiting wait loop");
    	}
    } 

    public MovieConverter(String media, String _output) throws InterruptedException, InvocationTargetException {
    	outputPath = _output;
        image = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().createCompatibleImage(width, height);
        image.setAccelerationPriority(1.0f);

        SwingUtilities.invokeAndWait(new Runnable() {
            @Override
            public void run() {
            	if(DRAW) {
	                frame = new JFrame("VLCJ Direct Video Test");
	                imagePane = new ImagePane(image);
	                imagePane.setSize(width, height);
	                imagePane.setMinimumSize(new Dimension(width, height));
	                imagePane.setPreferredSize(new Dimension(width, height));
	                frame.getContentPane().setLayout(new BorderLayout());
	                frame.getContentPane().add(imagePane, BorderLayout.CENTER);
	                frame.pack();
	                frame.setResizable(false);
	                frame.setVisible(true);
	
	                frame.addWindowListener(new WindowAdapter() {
	                    @Override
	                    public void windowClosing(WindowEvent evt) {
	                        mediaPlayer.release();
	                        factory.release();
	                        System.exit(0);
	                    }
	                });
            	}
            }

        });
        factory = new MediaPlayerFactory(new String[] {});
        mediaPlayer = factory.newDirectMediaPlayer(new TestBufferFormatCallback(), new TestRenderCallback());
        mediaPlayer.playMedia(media);
        System.out.println("Started to play back media.............");
    }
    
    public void nudge() {
    	if(nudged && !converting) {
    		//two nudges in a row! Time to convert
        	new Thread() {
        		public void run() {
        			mediaPlayer.stop();
        			converting = true;
        			convertAndWrite();
        		}
        	}.start();
    	}
    	nudged = true;
    }

    @SuppressWarnings("serial")
    private final class ImagePane extends JPanel {
        private final BufferedImage image;
        
        public ImagePane(BufferedImage image) {
            this.image = image;
        }

        @Override
        public void paint(Graphics g) {
            Graphics2D g2 = (Graphics2D)g;
            g2.drawImage(image, null, 0, 0);
            //store copy of image
            data.add(copyImage(image));
            //check for end of movie
            if(mediaPlayer != null) {
	            System.out.println(mediaPlayer.getPosition() + "/" + mediaPlayer.getLength());
	            nudged = false;
//	            if(!mediaPlayer.isPlaying() || mediaPlayer.getPosition() > 0.99) {
//	            	System.out.println("Media player has stopped playing.");
//	            	new Thread() {
//	            		public void run() {
//	            			mediaPlayer.stop();
//	            			converting = true;
//	            			convertAndWrite();
//	            		}
//	            	}.start();
//	            }
            }
        }
    }
    
    public static BufferedImage copyImage(BufferedImage source){
        BufferedImage b = new BufferedImage(source.getWidth(), source.getHeight(), source.getType());
        Graphics g = b.getGraphics();
        g.drawImage(source, 0, 0, Config.MOVIE_WIDTH, Config.MOVIE_HEIGHT, null);
        g.dispose();
        return b;
    }
    
    private void convertAndWrite() {
    	//original FPS
    	double srcFPS = mediaPlayer.getFps();
    	if(srcFPS == 0) {
    		srcFPS = 25;			
    		System.out.println("Could not get original FPS. Setting to 25.");
    	}
    	double dstFPS = Config.FPS;
    	double srcSPF = 1. / srcFPS;
    	double dstSPF = 1. / dstFPS;
		System.out.println("Starting conversion...");
    	int srcNumFrames = data.size();
    	int dstNumFrames = (int)(srcNumFrames * srcSPF / dstSPF);
		System.out.println(" --- Original     : " + srcNumFrames + " at " + srcFPS + " fps.");
		System.out.println(" --- Converting to: " + dstNumFrames + " at " + dstFPS + " fps.");
    	byte[][] finalData = new byte[dstNumFrames][];
    	int srcDataFrame = 0;
    	int dstDataFrame = 0;
    	double srcTimeInS = 0;
    	double dstTimeInS = 0;
    	//iterate through the original frames
    	for(BufferedImage frame : data) {
    		srcTimeInS = srcDataFrame * srcSPF;
    		dstTimeInS = dstDataFrame * dstSPF;
    		while(srcTimeInS >= dstTimeInS) {
    			if(dstDataFrame >= dstNumFrames) {
    				break;
    			}
    			finalData[dstDataFrame] = Util.getCompressedFrame(frame);
    			dstDataFrame++;
        		dstTimeInS = dstDataFrame * dstSPF;
        		System.out.println("Writing: " + srcDataFrame + " >> " + dstDataFrame);
    		}
    		srcDataFrame++;
    	}
    	//converted to compressed data. Now write.
    	try {
    		FileOutputStream fos = new FileOutputStream(new File(outputPath));
    		ObjectOutputStream oos = new ObjectOutputStream(fos);
    		oos.writeObject(finalData);
    	oos.close();
    	fos.close();
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
//    	System.exit(0);
    	mediaPlayer.release();
        factory.release();
        frame.dispose();
        finished = true;
    	System.out.println("Done conversion. Exiting!");
    }
    
    public static byte[][] read(String filename) {
    	try {
    		FileInputStream fis = new FileInputStream(new File(filename));
    		ObjectInputStream ois = new ObjectInputStream(fis);
    		byte[][] result = (byte[][])ois.readObject();
    		ois.close();
    		fis.close();
    		return result;
    	} catch(Exception e) {
    		e.printStackTrace();
    		return null;
    	}
    }
    
    public boolean finished() {
    	return finished;
    }

    private final class TestRenderCallback extends RenderCallbackAdapter {

        public TestRenderCallback() {
            super(((DataBufferInt) image.getRaster().getDataBuffer()).getData());
        }

        @Override
        public void onDisplay(DirectMediaPlayer mediaPlayer, int[] data) {
        	if(DRAW) imagePane.repaint();
        }
    }

    private final class TestBufferFormatCallback implements BufferFormatCallback {

        @Override
        public BufferFormat getBufferFormat(int sourceWidth, int sourceHeight) {
            return new RV32BufferFormat(width, height);
        }

    }
}