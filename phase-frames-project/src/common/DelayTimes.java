package common;

import java.util.Hashtable;
import java.util.Map;

public abstract class DelayTimes {


	//delay times stuff
	public static enum DelayTimesMode {
		FLAT, UP, DOWNUP, DOWN
	}
	
	public static Map<DelayTimesMode, double[]> delayTimesTemplates; 	//set of pre-generated delay times to interpolate between.
	
	static {
		int length = Config.numClients();
		int halflength = length / 2;
		delayTimesTemplates = new Hashtable<DelayTimesMode, double[]>();
		//FLAT
		double[] template = null;
		template = new double[length];
		for(int i = 0; i < length; i++) {
			template[i] = 0.5;
		}
		delayTimesTemplates.put(DelayTimesMode.FLAT, template);

		//DOWN
		template = new double[length];
		for(int i = 0; i < length; i++) {
			template[i] = 1. - (double)i / length;
		}
		delayTimesTemplates.put(DelayTimesMode.DOWN, template);
		//DOWNUP
		template = new double[length];
		for(int i = 0; i < halflength; i++) {
			template[i] = 1. - ((double)i / halflength);
		}
		for(int i = 0; i < halflength; i++) {
			template[i + halflength] = (double)i / halflength;
		}
		delayTimesTemplates.put(DelayTimesMode.DOWNUP, template);
		//UP
		template = new double[length];
		for(int i = 0; i < length; i++) {
			template[i] = (double)i / length;
		}
		delayTimesTemplates.put(DelayTimesMode.UP, template);
	}
	
}
