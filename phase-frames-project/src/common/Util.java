package common;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.HashSet;

import javax.imageio.ImageIO;

public abstract class Util {


	public static byte[] int2Bytes(int myInteger) {
		return ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(myInteger).array();
	}

	public static int bytes2Int(byte[] byteBarray) {
		return ByteBuffer.wrap(byteBarray).order(ByteOrder.LITTLE_ENDIAN).getInt();
	}
	
	public static byte[] long2Bytes(long myLong) {
		return ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putLong(myLong).array();
	}

	public static long bytes2Long(byte[] byteBarray) {
		return ByteBuffer.wrap(byteBarray).order(ByteOrder.LITTLE_ENDIAN).getLong();
	}

	public static String bytes2String(byte[] data) {
		return new String(data);
	}

	public static String bytes2String(byte[] data, int length) {
		return new String(Arrays.copyOf(data, length));
	}

	public static byte[] string2Bytes(String myString) {
		return myString.getBytes();
	}
	
	public static BufferedImage createImageBuffer() {
		return new BufferedImage(Config.MOVIE_WIDTH, Config.MOVIE_HEIGHT, Config.MOVIE_COLOUR_FORMAT);
	}
	
	public static void shutdown() {
		Config.stdout.println("Received shutdown command. Going down.");
		Config.stdout.close();
		Config.stderr.close();
		System.exit(0);
	}
	
	public static void sendMulticastPacket(DatagramSocket ds, byte[] packet, int port) {
		try {
			ds.send(new DatagramPacket(packet, packet.length, InetAddress.getByName(Config.MULTICAST_ADDRESS), port));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static String hostname2IP(String hostname) {
		InetSocketAddress sockAddr = new InetSocketAddress(hostname + ".local", 100);
		InetAddress addr = sockAddr.getAddress();
		if(addr == null) {
			return null;
		}
		return addr.getHostAddress();
	}
	
	public static void printImageIOFormats() {
	   HashSet<String> set = new HashSet<String>();
	    // Get list of all informal format names understood by the current set of registered readers
	    String[] formatNames = ImageIO.getReaderFormatNames();
	    for (int i = 0; i < formatNames.length; i++) {
	      set.add(formatNames[i].toLowerCase());
	    }
	    Config.stdout.println("Supported read formats: " + set);
	    set.clear();
	    // Get list of all informal format names understood by the current set of registered writers
	    formatNames = ImageIO.getWriterFormatNames();
	    for (int i = 0; i < formatNames.length; i++) {
	      set.add(formatNames[i].toLowerCase());
	    }
	    Config.stdout.println("Supported write formats: " + set);
	    set.clear();
	    // Get list of all MIME types understood by the current set of registered readers
	    formatNames = ImageIO.getReaderMIMETypes();
	    for (int i = 0; i < formatNames.length; i++) {
	      set.add(formatNames[i].toLowerCase());
	    }
	    Config.stdout.println("Supported read MIME types: " + set);
	    set.clear();
	    // Get list of all MIME types understood by the current set of registered writers
	    formatNames = ImageIO.getWriterMIMETypes();
	    for (int i = 0; i < formatNames.length; i++) {
	      set.add(formatNames[i].toLowerCase());
	    }
	    Config.stdout.println("Supported write MIME types: " + set); 
	}
	
	public static byte[] getCompressedFrame(BufferedImage bimg) {
		// Need these output streams to get image as bytes for UDP communication
		ByteArrayOutputStream baStream	= new ByteArrayOutputStream();
		BufferedOutputStream bos		= new BufferedOutputStream(baStream);
//		// Turn the BufferedImage into a JPG and put it in the BufferedOutputStream
		try {
			ImageIO.write(bimg, Config.MOVIE_COMPRESSION_FORMAT, bos);
		} catch (IOException e) {
			e.printStackTrace();
		}
		// Get the byte array.						//TODO is there a way to do this into an existing bytearray?
		byte[] fullData = baStream.toByteArray();
		//Here we have the compressed image now as a byte array.
		return fullData;
	}
	
	public static BufferedImage getDecompressedFrame(byte[] bytes) {
		if(bytes == null) {
			return null;
		}
		ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
		BufferedImage img = null;
		try {
			img = ImageIO.read(bais);
		} catch (Exception e) {
			e.printStackTrace(Config.stderr);
		}
		return img;
	}
	
	public static void convertRGBToGreyInPlace(int[] data) {
        /* RGB to GRAYScale conversion */
        for(int i=0; i < data.length; i++){
            int argb = data[i];
            int b = (argb & 0xFF);
            int g = ((argb >> 8 ) & 0xFF);
            int r = ((argb >> 16 ) & 0xFF);
            int grey = (r + g + b + g) >> 2 ; //performance optimized - not real grey!
            data[i] = (grey << 16) + (grey << 8) + grey;
        }
	}
	
	public static int convertRGBToGrey(int rgb) {
		int b = (rgb & 0xFF);
        int g = ((rgb >> 8 ) & 0xFF);
        int r = ((rgb >> 16 ) & 0xFF);
        return (r + g + b + g) >> 2 ; //performance optimized - not real grey!
	}
	
	public static int convertGreyToRGB(int grey) {
		int rgb = grey;
		rgb = (rgb << 8) + grey;
		rgb = (rgb << 8) + grey;
		return rgb;
	}
	
	public static String findMyHostname() {
		return EZShell.call("hostname").trim();
	}
	
	public static int findMyIndex() {
		String hostname = findMyHostname();
		if(!Config.DEVICES.containsKey(hostname)) {
			//panic!
			Config.stdout.println("MY HOSTNAME IS \"" + hostname + "\" BUT I CAN'T FIND MY ID!! ABORT");
			Config.stdout.println("You may need to add my hostname to Config.DEVICES.");
			System.exit(0);
		}
		int myID = Config.DEVICES.get(hostname);
		Config.stdout.println("Hostname: " + hostname + " (id=" + myID + ")");
		return myID;
	}

	public static byte[] createBlackFrame() {
		BufferedImage bim = createImageBuffer();
		for(int i = 0; i < Config.MOVIE_WIDTH; i++) {
			for(int j =0; j < Config.MOVIE_HEIGHT; j++) {
				bim.setRGB(i, j, 0);
			}
		}
		return getCompressedFrame(bim);
	}
}
