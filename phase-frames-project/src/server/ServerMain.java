package server;

import java.awt.Graphics;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import javax.swing.JFrame;
import javax.swing.JPanel;

import server.camera_receive.CameraReceive;
import server.camera_receive.CameraReceiveJPG;
import server.camera_receive.CameraReceiveVLC;
import uk.co.caprica.vlcj.discovery.NativeDiscovery;
import common.Config;
import common.Util;

public class ServerMain extends Application {
	
	//
	public static final boolean verbose = false;
	public static final boolean RECEIVE = true;
	public static final boolean SEND = true;
	public static final boolean DRAW = true;
	public static final boolean USE_JPEG_RECEIVER = true;
	public static final boolean SPLIT_PACKETS = false;
	public static final boolean UPDATE_STATE = true;
	public static final boolean USE_DELAYS = true;
	public static final boolean KINECT_DELAYS = true;
	public static final boolean PLAY_AUDIO = true;
	public static final boolean USE_JACK_AUDIO = true;
	public static final boolean ONE_AUDIO_VOICE = false;
	public static final boolean LOOP_ON_INPUT = true;
	public static final boolean CONVERT_TO_GREY = true;
	public static final boolean CROSSFADE = true;
	
	public static byte[] blackFrame;

	FrameSender frameSender;
	public MessageSender messageSender;
	int time;
	JFrame window;
	JPanel drawPanel;
	
	byte[] currentFrameCompressed;
	
	CameraReceive[] cameraReceivers;
	public MovieMaker movieMaker;
	Composition composition;
	public boolean ready = false;
	boolean[] framesRequested;
	KinectGrabber kinectGrabber;
	
	public static void main(String[] args) {
		//this is necessary to make the VLC receiving work, part of VLCJ library.
		if(!USE_JPEG_RECEIVER) {
			new NativeDiscovery().discover();
		}
		Application.launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		//basic data structures
		framesRequested = new boolean[Config.numClients()];
		blackFrame = Util.createBlackFrame();
		//set up the Composition and MovieMaker
		composition = new Composition(this);
		movieMaker = new MovieMaker(this);
		kinectGrabber = new KinectGrabber();
		//sending stuff
		frameSender = new FrameSender();
		messageSender = new MessageSender(this);
		//set up the camera listeners
		cameraReceivers = new CameraReceive[Config.numClients()];
		for(int i = 0; i < Config.numClients(); i++) {
			if(RECEIVE) {
				if(USE_JPEG_RECEIVER) {
					cameraReceivers[i] = new CameraReceiveJPG(ServerMain.this, Config.HOSTNAMES[i]);	
				} else {
					cameraReceivers[i] = new CameraReceiveVLC(ServerMain.this, Config.HOSTNAMES[i]);
				}
			}
		}
		Config.stdout.println("Done creating camera receivers.");
		//video window
		if(DRAW) {
			window = new JFrame("Phase Frames Server");
			window.setSize(1000, 500);
			drawPanel = new JPanel() {
				private static final long serialVersionUID = 1L;
				public void paintComponent(Graphics g) {
					if(DRAW && currentFrameCompressed != null) {
						g.drawImage(Util.getDecompressedFrame(currentFrameCompressed), 0, 0, null);	
					}
				}
			};
			window.setContentPane(drawPanel);
			window.setVisible(true);
		}
		//main run thread
		Timer runThread = new Timer(true);
		runThread.scheduleAtFixedRate(
			new TimerTask() {
				public void run() {
					update(); 
				}
			}, 0, (long)Config.MS_PER_FRAME);
		//control window
		Group root = new Group();
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.show();
		//controls
		Button camOnOff = new Button();
		camOnOff.setOnAction(new EventHandler<ActionEvent>() {
		    @Override public void handle(ActionEvent e) {
		        messageSender.messageCPP("startcamera");
		    }
		});
		root.getChildren().add(camOnOff);
		//now ready
		ready = true;
	}

	void update() {
		long timeMS = System.currentTimeMillis();
		kinectGrabber.update();
		//reset frames requested
		Arrays.fill(framesRequested, false);
		if(verbose) Config.stdout.println("Creating frame " + time);
		composition.update();
		movieMaker.update();
		//once frame is created, multicast it
		if(currentFrameCompressed != null) {
			if(SEND) {
				frameSender.sendFrame(currentFrameCompressed, time);
//				if(currentFrameCompressed == blackFrame) {
//					currentFrameCompressed = null;
//				}
			}
		}
		//also multicast the updated delay times
		multicastDelayTimes();
		//and draw
		if(DRAW) {
			window.repaint();
			window.revalidate();
		}
		//check to see that we're still getting video
		if(RECEIVE) {
			//int fiveFramesMS = (int)(Config.MS_PER_FRAME * 5);
			for(int i = 0; i < cameraReceivers.length; i++) {
				CameraReceive cr = cameraReceivers[i];
				if(cr.timeSinceLastUpdate() > 5000) {
					Config.stderr.println("No update from camera " + i +", trying to reconnect");
					cr.tryReconnect();
				}
			}
		}
		//and count time
		time++;
		long dur = (System.currentTimeMillis() - timeMS);
		if(verbose) Config.stdout.println("Overall update loop: " + dur + " / " + Config.MS_PER_FRAME);
	}
	
	boolean camerasReady() {
		for(CameraReceive cameraReceive : cameraReceivers) {
			if(!cameraReceive.isReady()) {
				return false;
			}
		}
		return true;
	}
	
	void setCurrentFrame(byte[] compressedFrame) {
		currentFrameCompressed = compressedFrame;
	}
	
	//note by setting currentFrameCompressed to null this stops any more frame sending.
	void setBlack() {
		currentFrameCompressed = blackFrame;
	}
	
	void setFrameRequested(int cameraID) {
		framesRequested[cameraID] = true;
	}
	
	void setFrameRequestedAll() {
		Arrays.fill(framesRequested, true);
	}
	
	void multicastDelayTimes() {
			StringBuffer delayTimesString = new StringBuffer("d");
			for(int i = 0; i < framesRequested.length; i++) {
				delayTimesString.append(framesRequested[i] ? "t" : "f");
			}
			
			for(int i = 0; i < movieMaker.delayTimesFrames.length; i++) {
				int delayTimeInt = 1;
				if(USE_DELAYS) {
					delayTimeInt = (int)Math.round(movieMaker.delayTimesFrames[i]) + 1;//reduce to "int" here and add 1 frame for safety.
				}
				delayTimesString.append(" " + delayTimeInt); 
			}
			delayTimesString.append(" ");
			messageSender.messageCPP(delayTimesString.toString());
	}

}
