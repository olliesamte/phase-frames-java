package server;

import java.util.ArrayList;

import net.beadsproject.beads.core.AudioContext;
import net.beadsproject.beads.data.Sample;
import net.beadsproject.beads.data.SampleManager;
import net.beadsproject.beads.events.KillTrigger;
import net.beadsproject.beads.ugens.Envelope;
import net.beadsproject.beads.ugens.Gain;
import net.beadsproject.beads.ugens.GranularSamplePlayer;
import net.beadsproject.beads.ugens.Reverb;
import net.beadsproject.beads.ugens.SamplePlayer;

import org.jaudiolibs.beads.AudioServerIO;

import server.audio.Audio;
import server.audio.AudioVoice;
import common.Config;
import common.DelayTimes;

public class Composition {

	public static final boolean verbose = false;

	// the server
	final ServerMain serverMain;

	// audio stuff
	AudioContext ac;
	Envelope gainEnv;

	Gain voicesGain;
	Envelope voicesGainEnv;

	Gain musicGain;
	Envelope musicGainEnv;

	AudioVoice[] voices;
	ArrayList<Gain> liveVoices;
	boolean wasResting;

	GranularSamplePlayer music1;
	GranularSamplePlayer music2;
	GranularSamplePlayer music3;

	public Composition(ServerMain _serverMain) {
		this.serverMain = _serverMain;
		// load audio
		Audio.loadAudio();
		// start audio processing
		liveVoices = new ArrayList<Gain>();
		if (ServerMain.PLAY_AUDIO) {
			if (ServerMain.USE_JACK_AUDIO) {
				ac = new AudioContext(new AudioServerIO.Jack(), 512);
			} else {
				ac = new AudioContext();
			}
			gainEnv = new Envelope(ac, 1);
			ac.out.setGain(gainEnv);
			
			voicesGainEnv = new Envelope(ac, 1);
			voicesGain = new Gain(ac, 2, voicesGainEnv);
			ac.out.addInput(voicesGain);

			musicGainEnv = new Envelope(ac, 0.9f);
			musicGain = new Gain(ac, 2, musicGainEnv);
			ac.out.addInput(musicGain);

			ac.start();
			// voices
			voices = new AudioVoice[Config.numClients()];
			for (int i = 0; i < voices.length; i++) {
				voices[i] = new AudioVoice(ac, i);
				voicesGain.addInput(voices[i].getOutput());
				// System.out.println("X");
				// System.out.println("Setting output to " + Config.MEDIA_DIR +
				// "/audio/" + Config.MOVIE_FILENAMES[0] + ".wav");
				// voices[i].setSample(SampleManager.sample(Config.MEDIA_DIR +
				// "/audio/" + Config.MOVIE_FILENAMES[0] + ".wav"));
				// voices[i].start(0);
			}
		}
		setupMusic();
	}

	public void update() {
		// System.out.println("Current Segment " +
		// serverMain.movieMaker.currentMovieSegment);
		updateParams();
		updateState();
		updateDelayTimes();
		updateAudio();
	}

	void setupMusic() {
		music1 = new GranularSamplePlayer(ac, SampleManager.sample(Config.MEDIA_DIR + "/audio/synth1.wav"));
		music2 = new GranularSamplePlayer(ac, SampleManager.sample(Config.MEDIA_DIR + "/audio/synth2.wav"));
		music3 = new GranularSamplePlayer(ac, SampleManager.sample(Config.MEDIA_DIR + "/audio/marimba.wav"));

		music1.setLoopType(SamplePlayer.LoopType.LOOP_FORWARDS);
		music2.setLoopType(SamplePlayer.LoopType.LOOP_FORWARDS);
		music3.setLoopType(SamplePlayer.LoopType.LOOP_FORWARDS);

		music1.getGrainSizeUGen().setValue(50);
		music1.getGrainIntervalUGen().setValue(20);
		music1.getRandomnessUGen().setValue(0.01f);

		music2.getGrainSizeUGen().setValue(140);
		music2.getGrainIntervalUGen().setValue(90);
		music2.getRandomnessUGen().setValue(0.15f);

		music3.getGrainSizeUGen().setValue(100);
		music3.getGrainIntervalUGen().setValue(60);
		music3.getRandomnessUGen().setValue(0.05f);

		musicGain.addInput(music1);
		musicGain.addInput(music2);
		musicGain.addInput(music3);
	}

	void updateParams() {
		// playback speed
		serverMain.movieMaker.playbackRate = scale(normSine(serverMain.time * 0.01), 0.6, 1.2);
		// rotation speed
		serverMain.movieMaker.rotationRateHz = scale(Math.pow(normSine(serverMain.time * 0.0134), 5), 0.2, 0.7);
		if (verbose)
			Config.stdout.println("Playback rate: " + serverMain.movieMaker.playbackRate);
		if (verbose)
			Config.stdout.println("Playback pos (ms): " + serverMain.movieMaker.moviePlayheadMS + " ms");
		if (verbose)
			Config.stdout.println("Rotation rate: " + serverMain.movieMaker.rotationRateHz + " Hz");
		if (verbose)
			Config.stdout.println("Rotation pos: " + serverMain.movieMaker.rotation);
	}

	static double normSine(double x) {
		return 0.5 * Math.sin(x) + 0.5;
	}

	static double scale(double x, double low, double high) {
		return x * (high - low) + low;
	}

	void updateState() {
		if (serverMain.movieMaker.isResting()) {
			if(wasResting == false ) {
				Sample s = SampleManager.sample(Config.MEDIA_DIR + "/audio/guit" + (Config.RNG.nextInt(10) + 1) + ".wav");
				SamplePlayer sp = new GranularSamplePlayer(ac, s);
				sp.getRateUGen().setValue(0.15f);
				sp.getPitchUGen().setValue(0.75f);
				Envelope e = new Envelope(ac, 6f);
				Gain g = new Gain(ac, 2, e);
				g.addInput(sp);
				e.addSegment(4f, 2000f);
				e.addSegment(2f, 5000f);
				e.addSegment(0, 5000f, new KillTrigger(g));
				ac.out.addInput(g);
			}
			wasResting = true;
		}
		if (serverMain.movieMaker.isCamera()) {
			if (ServerMain.PLAY_AUDIO)
				voicesGainEnv.addSegment(1, 500); // AND HERE... TEST THIS
			// do nothing
		}
		if (serverMain.movieMaker.isMovie() && ServerMain.UPDATE_STATE) {
			if (wasResting) {
				// notify for audio state update
				audioStateUpdate();
				wasResting = false;
			}
			// end the loop if we are at the last allocated frame (different
			// cases for camera or no camera)
			boolean endOfLoop = (serverMain.movieMaker.playbackFrame >= serverMain.movieMaker.movieFrames[serverMain.movieMaker.currentMovieSegment].length - 1); // and
																																									// last
																																									// movie
																																									// frame
			// if end of loop then switch
			if (endOfLoop) {
				// switch, increment the movie id
				serverMain.movieMaker.currentMovieSegment = (serverMain.movieMaker.currentMovieSegment + 1) % serverMain.movieMaker.movieFrames.length;
				// reset the playback frame
				serverMain.movieMaker.moviePlayheadMS = 0;
				// switch to rest
				serverMain.movieMaker.timeSinceRest = 0;
				serverMain.setBlack();
				if (ServerMain.PLAY_AUDIO) {
					voicesGainEnv.clear();
					voicesGainEnv.addSegment(0, 3000);
					for (AudioVoice av : voices) {
						av.stop(); // LAZY
					}
				}
			}
		}
		serverMain.movieMaker.timeSinceRest++;
	}

	void audioStateUpdate() {
		// audio processing on switch
		if (!ServerMain.PLAY_AUDIO)
			return;
		if (serverMain.movieMaker.isMovie()) {
			for (AudioVoice av : voices) {
				av.setSample(SampleManager.sample(Config.MEDIA_DIR + "/audio/" + Config.MOVIE_FILENAMES[serverMain.movieMaker.currentMovieSegment] + ".wav"));
				av.start(0);
			}
		} else {
			for (AudioVoice av : voices) {
				av.stop();
			}
		}
	}

	void updateAudio() {
		if (!ServerMain.PLAY_AUDIO) {
			return;
		}
		// update audio delays and rates
		for (int i = 0; i < voices.length; i++) {
			voices[i].updateDelayTime(Config.MS_PER_FRAME * serverMain.movieMaker.delayTimesFrames[i]);
			voices[i].setPlaybackRate(serverMain.movieMaker.playbackRate);
		}

		// do the clicks
		if (serverMain.movieMaker.isCamera() && serverMain.movieMaker.prevCamera != serverMain.movieMaker.cameraID1) {
			// make a click
			for(int i = 0; i < Config.numClients(); i++) {
				voices[i].oneShot(Audio.singleClick, 2f);
			}

		}
		// do the music speed
		if (!serverMain.movieMaker.isMovie()) {
			music1.getRateUGen().setValue(-0.05f);
			music2.getRateUGen().setValue(-0.05f);
			music3.getRateUGen().setValue(-0.05f);
		} else {
			music1.getRateUGen().setValue((float) serverMain.movieMaker.playbackRate);
			music2.getRateUGen().setValue((float) serverMain.movieMaker.playbackRate);
			music3.getRateUGen().setValue((float) serverMain.movieMaker.playbackRate);
		}
	}

	synchronized void addVoice(Sample s) {
		GranularSamplePlayer gsp = new GranularSamplePlayer(ac, s);
		Envelope e = new Envelope(ac, 1);
		Gain g = new Gain(ac, 2, e);
		g.addInput(gsp);
		ac.out.addInput(g);
		liveVoices.add(g);
	}

	synchronized void killVoices() {
		for (Gain g : liveVoices) {
			((Envelope) g.getGainUGen()).addSegment(0, 500, new KillTrigger(g));
		}
		liveVoices.clear();
	}

	void updateDelayTimes() {
		if (!ServerMain.USE_DELAYS) {
			return;
		}
		// if (serverMain.movieMaker.isCamera()) {
		// Arrays.fill(serverMain.movieMaker.delayTimesFrames, 0);
		// } else {
		if (ServerMain.KINECT_DELAYS) {
			int halfClients = Config.numClients() / 2;
			int centreID = (int) ((serverMain.kinectGrabber.posX + 1) * 0.5 * Config.numClients());
			int centreScaling = Math.abs(halfClients - centreID) + halfClients;
			double strength = serverMain.kinectGrabber.posY;

			// System.out.println("Kinect " + strength + " " + centreID);

			// System.out.print("Delays ");
			for (int i = 0; i < serverMain.movieMaker.delayTimesFrames.length; i++) {
				int distanceToCentre = Math.abs(i - centreID);
				double offsetScale = distanceToCentre / (double) centreScaling;
				serverMain.movieMaker.delayTimesFrames[i] = Config.MAX_DELAY_FRAMES * strength * offsetScale;
				// System.out.print(" " +
				// serverMain.movieMaker.delayTimesFrames[i]);
			}
			// System.out.println();
		} else {

			// cycles between 4 modes:
			// -- \ \/ /
			int cycleTime = serverMain.time % Config.DELAY_TIME_CYCLE_PERIOD;
			if (cycleTime == 0) {
				// change mode
				serverMain.movieMaker.incrementDelayTimesMode();
			}
			// within each mode we do a smooth transition from previous mode
			// to new mode
			double interp = (double) cycleTime / Config.DELAY_TIME_CYCLE_PERIOD;
			double[] array1 = DelayTimes.delayTimesTemplates.get(serverMain.movieMaker.delayTimesMode);
			double[] array2 = DelayTimes.delayTimesTemplates.get(serverMain.movieMaker.nextDelayTimesMode);
			// Config.stdout.print("Delays: ");
			for (int i = 0; i < serverMain.movieMaker.delayTimesFrames.length; i++) {
				double delay = Config.MAX_DELAY_FRAMES * ((1. - interp) * array1[i] + interp * array2[i]);
				serverMain.movieMaker.delayTimesFrames[i] = delay;
				// Config.stdout.print(Math.round(delay) + " ");
			}
			// Config.stdout.println();
		}
		// }
	}
}
