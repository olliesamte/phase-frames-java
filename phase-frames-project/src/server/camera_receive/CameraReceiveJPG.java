package server.camera_receive;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.HashMap;

import javax.swing.JFrame;

import server.ServerMain;
import common.Config;
import common.UDPPacketToFrameBuilder;
import common.Util;

public class CameraReceiveJPG implements CameraReceive {

	//
	public static final boolean verbose = false;

	//owner
	final ServerMain serverMain;
	String hostname;
	int id;
	int port;
	int[] rgb;
	
	long lastUpdateTime = 0;
	
	//the multicast socket for listening for video packets
	DatagramSocket ds;
	// A byte array to read into (max size of 65536, could be smaller)
	byte[] buffer = new byte[65536];
	
	//data structure to store UDP buffers whilst building picture
	HashMap<Integer, UDPPacketToFrameBuilder> frameBuffers = new HashMap<Integer, UDPPacketToFrameBuilder>();
	
	//recycleable storage (may not be serving any purpose!)
	byte[] numberBuffer = new byte[4];
	byte[] tempFrameBuffer = new byte[1000000];
	
	public CameraReceiveJPG(ServerMain _serverMain, String _hostname) {
		this.serverMain = _serverMain;
		this.hostname = _hostname;
    	this.id = Config.DEVICES.get(hostname);
    	this.port = Config.CAMERA_STREAM_START_PORT + id;  
//    	this.address = Util.hostname2IP(hostname);
		try {
			ds = new DatagramSocket(Config.CAMERA_STREAM_START_PORT + id);
//			ms.joinGroup(InetAddress.getByName(Config.MULTICAST_ADDRESS));
		} catch (IOException e) {
			e.printStackTrace();
		}
		//set up own thread reading from the stream
		new Thread() {
			public void run() {
				while (true) {
					if(serverMain.ready) {
						checkForImage();	//is this blocking??
					}
					try {
						Thread.sleep(5);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}.start();
		Config.stdout.println("Camera receiver " + id + " ready.");
	}
	
	void checkForImage() {
		if(verbose) Config.stdout.println("Waiting for datagram... " + id);
		DatagramPacket p = new DatagramPacket(buffer, buffer.length);
		try {
			ds.receive(p);
		} catch (IOException e) {
			e.printStackTrace();
		}
		byte[] imgData;
		if(ServerMain.SPLIT_PACKETS) {
			byte[] data = p.getData().clone(); // any point to recycle this?
			// /////
			// begin new byte code
			// Read incoming data into a ByteArrayInputStream
			for (int i = 0; i < 4; i++) {
				numberBuffer[i] = data[i];
			}
			int frameNum = Util.bytes2Int(numberBuffer);
			for (int i = 0; i < 4; i++) {
				numberBuffer[i] = data[i + 4];
			}
			int packetNum = Util.bytes2Int(numberBuffer);
			for (int i = 0; i < 4; i++) {
				numberBuffer[i] = data[i + 8];
			}
			int totalPackets = Util.bytes2Int(numberBuffer);
			if (frameBuffers.containsKey(frameNum)) {
				frameBuffers.get(frameNum).AddPacket(data, packetNum, p.getLength());
			} else {
				UDPPacketToFrameBuilder newBuffer = new UDPPacketToFrameBuilder(frameNum, totalPackets);
				newBuffer.AddPacket(data, packetNum, p.getLength());
				frameBuffers.put(frameNum, newBuffer);
			}
			if (!frameBuffers.get(frameNum).IsBufferReady()) {
				return;
			}
			imgData = frameBuffers.get(frameNum).ConstructFrame(); // recycle this?
			frameBuffers.remove(frameNum);
			// /////////////////////
			// end receiver code (this may have exited if we don't have a complete
			// image).
			// if we are still here then imgData is full.
		} else {
			imgData = p.getData();
			if(verbose)  Config.stdout.println("Got packet of size " + p.getLength() + ", id=" + id);
		}
		synchronized(serverMain) {
//			BufferedImage bim = Util.getDecompressedFrame(imgData);
//			rgb = bim.getRGB(0, 0, Config.MOVIE_WIDTH, Config.MOVIE_HEIGHT, null, 0, Config.MOVIE_WIDTH);
			serverMain.movieMaker.currentCameraFrames[id] = imgData.clone();
//			System.out.println("Got data");
			lastUpdateTime = System.currentTimeMillis();
		}
	}
	
	public int[] getFrame() {		
		int[] result = rgb;
		rgb = null;
		return result;
	}
	
	public void clearFrame() {
		rgb = null;
	}

	@Override
	public boolean isReady() {
		return true;
	}

	@Override
	public long timeSinceLastUpdate() {
		return System.currentTimeMillis() - lastUpdateTime;
	}
	
	public void tryReconnect() {
		//NOT NEEDED?
	}

	//just a test
	public static void main(String[] args) throws Exception {
		// A byte array to read into (max size of 65536, could be smaller)
		JFrame frame = new JFrame();
		frame.setSize(Config.MOVIE_WIDTH, Config.MOVIE_HEIGHT);
		frame.setVisible(true);
		byte[] buf = new byte[65536];
		String hostname = "b827ebd77d1c";
    	int id = Config.DEVICES.get(hostname);
    	int port = Config.CAMERA_STREAM_START_PORT + id;
		DatagramSocket s = new DatagramSocket(port);
		//set up own thread reading from the stream
		new Thread() {
			public void run() {
				while (true) {
					try {
						DatagramPacket packet = new DatagramPacket(buf, buf.length);
						System.out.println("Waiting...");
						s.receive(packet);
						int len = packet.getLength();
						byte[] data = packet.getData();
						BufferedImage bim = Util.getDecompressedFrame(data);
						frame.getGraphics().drawImage(bim, 0, 0, null);
						System.out.println("Got data of length " + len);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}.start();
	}

}
