package server.camera_receive;

public interface CameraReceive {

	boolean isReady();
	long timeSinceLastUpdate();
	void tryReconnect();
	int[] getFrame();
	void clearFrame();

}
