package server.camera_receive;
import server.ServerMain;
import uk.co.caprica.vlcj.component.DirectMediaPlayerComponent;
import uk.co.caprica.vlcj.player.direct.BufferFormat;
import uk.co.caprica.vlcj.player.direct.BufferFormatCallback;
import uk.co.caprica.vlcj.player.direct.DirectMediaPlayer;
import uk.co.caprica.vlcj.player.direct.RenderCallback;
import uk.co.caprica.vlcj.player.direct.RenderCallbackAdapter;
import uk.co.caprica.vlcj.player.direct.format.RV32BufferFormat;
import common.Config;
import common.Util;

public class CameraReceiveVLC implements CameraReceive {
	
	//
	public static final boolean verbose = false;

    private DirectMediaPlayerComponent mediaPlayerComponent;
    final String hostname;
    final int id;
    String address;
    final int port;
    final ServerMain serverMain;
    Thread theThread;
    
    long lastUpdateTime = -1;
    
    int[] rgb;
    
    boolean ready = false;

    public CameraReceiveVLC(ServerMain _serverMain, String _hostname) {
    	this.serverMain = _serverMain;
    	this.hostname = _hostname;
    	this.id = Config.DEVICES.get(hostname);
    	this.port = Config.CAMERA_STREAM_START_PORT + id;   
    	reconnectInThread();
    }
    
    public boolean isReady() {
    	return ready;
    }
    
    public void tryReconnect() {
    	new Thread() {
    		public void run() {
    			reconnectInThread();
    		}
    	}.start();
    }
    
    private void reconnectInThread() {
    	//first tell the client to reboot
//    	serverMain.messageSender.messageJava("r " + id);			//do this?
    	//then wait
    	try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    	//then look again for the connection
    	this.address = Util.hostname2IP(hostname);
    	if(this.address == null) {
    		Config.stdout.println("WARNING: COULD NOT GET HOST: " + hostname + "(" + address + ":" + port + ")");
    		return;
    	}
    	theThread = new Thread() {
    		public void run() {
    			BufferFormatCallback bufferFormatCallback = new BufferFormatCallback() {
    	            @Override
    	            public BufferFormat getBufferFormat(int sourceWidth, int sourceHeight) {
    	                return new RV32BufferFormat(Config.MOVIE_WIDTH, Config.MOVIE_HEIGHT);
    	            }
    	        };
    	        if(mediaPlayerComponent != null) {
        	        mediaPlayerComponent.release();
    	        }
    	        mediaPlayerComponent = new DirectMediaPlayerComponent(bufferFormatCallback) {
    	            @Override
    	            protected RenderCallback onGetRenderCallback() {
    	                return new PhaseFramesRenderCallbackAdapter();
    	            }
    	        };
    	        Config.stdout.println("Streaming from address: " + address + ", port   : " + port);
    	        DirectMediaPlayer player = mediaPlayerComponent.getMediaPlayer();
    	        player.playMedia("rtsp://" + address + ":" + port + "/");
    	        player.pause();		//why is this here?
    	        lastUpdateTime = 0;
    		}
    	};
    	theThread.start(); 
    }

    private class PhaseFramesRenderCallbackAdapter extends RenderCallbackAdapter {
        private PhaseFramesRenderCallbackAdapter() {
            super(new int[Config.MOVIE_WIDTH * Config.MOVIE_HEIGHT]);
        }

        @Override
        protected void onDisplay(DirectMediaPlayer mediaPlayer, int[] rgbBuffer) {
            if(verbose) Config.stdout.println("Got frame from " + hostname + ":" + port);
            //store frame for use by server
            if(verbose) Config.stdout.println("Putting frame into memory, frame is " + rgbBuffer);
            //just copy the data, as quickly as possible
            synchronized (rgbBuffer) {
//            	long time = System.currentTimeMillis();
            	rgb = rgbBuffer.clone();
//                System.arraycopy(rgbBuffer, 0, serverMain.movieMaker.incomingFrames[id], 0, rgbBuffer.length);	
//                Config.stdout.println("Time to copy frame: " + (System.currentTimeMillis() - time));
                lastUpdateTime = System.currentTimeMillis();
			}            
        }
    }
        
    public int[] getFrame() {
    	return rgb;
    }
    
	
	public void clearFrame() {
		rgb = null;
	}

    
	@Override
	public long timeSinceLastUpdate() {
		if(lastUpdateTime == -1) {
			return 0;
		} else {
			return System.currentTimeMillis() - lastUpdateTime;			
		}
	}
    
}

