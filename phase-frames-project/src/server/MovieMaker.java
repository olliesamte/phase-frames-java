package server;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.Arrays;

import common.Config;
import common.DelayTimes;
import common.MovieConverter;
import common.Util;


public class MovieMaker {

	public static final boolean verbose = false;
	

	final ServerMain serverMain;
	
	//playback stuff
	int timeSinceRest;							//count frames elapsed since starting to rest
	int currentMovieSegment = 0; 				//0-n = any of the video files
	double playbackRate = 7;					//speed, 1 = normal
	double moviePlayheadMS = 0;
	int playbackFrame;
	
	byte[][][] movieFrames;						//movie frame data indexed by [movieID][frameNumber][data]
	
	double[] delayTimesFrames;						//the delay time for playback on each device, measured in frames
	DelayTimes.DelayTimesMode delayTimesMode 		//the delay times mode (FLAT, UP, UPDOWN, DOWN)
		= DelayTimes.DelayTimesMode.FLAT;
	DelayTimes.DelayTimesMode nextDelayTimesMode 	//the next delay times mode to transition to
		= DelayTimes.DelayTimesMode.UP;
	
	//rotation and camera grab stuff
	double rotationRateHz = 0.5; 		 		//fraction of circle per second
	int rotationDirection = 1;					//direction
	double rotation = 0;						//fraction of circle
	double interpolation;
	int cameraID1, cameraID2, prevCamera;		//IDs of the current 2 cameras to use, based on rotation, plus a note of the previous camera.
	public byte[][] currentCameraFrames;			//store for current camera frame
	BufferedImage mixedFrame = Util.createImageBuffer();
	BufferedImage mixedFrameTmp = Util.createImageBuffer();
	
	public MovieMaker(ServerMain _serverMain) {
		this.serverMain = _serverMain;
		//set up data structures
		delayTimesFrames = new double[Config.numClients()];
		currentCameraFrames = new byte[Config.numClients()][];
		//load the movie data
		readMovies();
		//initial update rotation to init the correct values
		updateRotation();
	}
	
	void readMovies() {
		movieFrames = new byte[Config.MOVIE_FILENAMES.length][][];
		for(int i = 0; i < movieFrames.length; i++) {
			movieFrames[i] = MovieConverter.read(Config.MEDIA_DIR + "/raw/" + Config.MOVIE_FILENAMES[i]);
		}
	}
	
	void update() {
//		long time = System.currentTimeMillis();
		//every so often take a snapshot
		if(serverMain.time % Config.CAMERA_SHOT_INTERVAL_FRAMES == 0) {
			serverMain.setFrameRequestedAll();
		}
		updatePlayback();
		updateRotation();
		if(!isResting()) prepareFrame();
	}
	
	boolean isResting() {
		return timeSinceRest < Config.TIME_RESTING_FRAMES;
	}
	
	boolean isCamera() {
		return timeSinceRest >= Config.TIME_RESTING_FRAMES && timeSinceRest < (Config.TIME_RESTING_FRAMES + Config.CAMERA_PLAY_DURATION_FRAMES);
	}
	
	boolean isMovie() {
		return timeSinceRest >= (Config.TIME_RESTING_FRAMES + Config.CAMERA_PLAY_DURATION_FRAMES);
	}

	void updatePlayback() {
		//manage movie playback
		moviePlayheadMS += playbackRate * Config.MS_PER_FRAME;
		playbackFrame = (int)(moviePlayheadMS * Config.FRAMES_PER_MS);
	}
	
	void updateRotation() {
		double rotationIncrement = rotationDirection * rotationRateHz / Config.FPS;
		rotation += rotationIncrement;
		if(rotation >= 1) {
			rotation = 1.999 - rotation;		//make sure it is never 1
			rotationDirection = -rotationDirection;
		} else if(rotation < 0) {
			rotation = -rotation;
			rotationDirection = -rotationDirection;
		}
		double squashedRotation = Math.tanh(rotation * 4 - 2) * 0.5 + 0.5;
		double cameraPos = squashedRotation * (Config.numClients() - 1);
		double progress = 0;
//		System.out.println("Rotation: " + rotation );
//		System.out.println("Camera pos: " + cameraPos );
		if(rotationDirection > 0) {
			prevCamera = cameraID1;
			cameraID1 = (int)(cameraPos);
			cameraID2 = (cameraID1 + 1);
			progress = cameraPos - (int)cameraPos;
		} else {
			prevCamera = cameraID1;
			cameraID2 = (int)(cameraPos);
			cameraID1 = (cameraID2 + 1);
			progress = 1. - (cameraPos - (int)cameraPos);
		}
		if(Config.CROSSFADE_FRACTION == 0) {
			interpolation = 1;
		}
		else {
			interpolation = Math.min(1, (progress / Config.CROSSFADE_FRACTION));
		}
		
	}

	void prepareFrame() {
		//this needs to ultimately set serverMain.currentFrameCompressed to new state
		if(isCamera()) { //use cameras
			synchronized(serverMain) {
//				System.out.println("Sending frame from camera " + cameraID);
//				serverMain.setCurrentFrame(currentCameraFrames[cameraID]);
				getCrossfadeFrame();
			}
		} else {	//use movies
			int playbackFrameWrapped = fullWrap(playbackFrame, movieFrames[currentMovieSegment].length);
			byte[] compressedFameData = movieFrames[currentMovieSegment][playbackFrameWrapped];
			//sent it
			serverMain.setCurrentFrame(compressedFameData);
		}
	}
	
	void getCrossfadeFrame() {
		if(Config.CROSSFADE_FRACTION != 0 && interpolation < 1 && ServerMain.CROSSFADE) {
			if(currentCameraFrames[cameraID1] == null || currentCameraFrames[cameraID2] == null) {
				return;
			}
			int nextGreyVal, nextRGBVal, value1, value2;
			double neginterp = 1. - interpolation;
			BufferedImage cameraFrame1 = Util.getDecompressedFrame(currentCameraFrames[cameraID1]);
			BufferedImage cameraFrame2 = Util.getDecompressedFrame(currentCameraFrames[cameraID2]);
			//mix and store into next buffer point
			for(int j = 0; j < Config.MOVIE_HEIGHT; j++) {
				for(int i = 0; i < Config.MOVIE_WIDTH; i++) {
					value1 = cameraFrame1.getRGB(i, j);
					value2 = cameraFrame2.getRGB(i, j);
					if(ServerMain.CONVERT_TO_GREY) {
						value1 = Util.convertRGBToGrey(value1);
						value2 = Util.convertRGBToGrey(value2);
						nextGreyVal = (int)(value1 * neginterp + value2 * interpolation);
						nextRGBVal = Util.convertGreyToRGB(nextGreyVal);
					} else {
						//TODO - this is junk, we need to convert to RGB before doing an interp!
						nextRGBVal = (int)(value1 * neginterp + value2 * interpolation);
					}
					mixedFrameTmp.setRGB(i, j, nextRGBVal);
				}
			}
		} else {
			if(currentCameraFrames[cameraID2] == null) {
				return;
			}
			mixedFrameTmp = Util.getDecompressedFrame(currentCameraFrames[cameraID2]);
		}
//		//do some colour fun here? Too expensive?
//		for(int i = 0; i < Config.MOVIE_WIDTH; i++) {
//			for(int j = 0; j < Config.MOVIE_HEIGHT; j++) {
//				int rgb = mixedFrameTmp.getRGB(i, j);
////				int red = 	(rgb & 0x00ff0000) >> 16;
////				int green = (rgb & 0x0000ff00) >> 8;
//// 				int blue = 	rgb & 0x000000ff;
//				Color c1 = new Color(rgb);
//				Color c2 = new Color(c1.getRed(), 0, 0);//c1.getGreen() / 255f, c1.getBlue() / 255f);
//				mixedFrameTmp.setRGB(i, j, c2.getRGB());
//			}
//		}
		//do scaling
		double scale = 0.6 - serverMain.kinectGrabber.posY;		//number between 1 and 2
		if(serverMain.kinectGrabber.posY > 0.6) {
			scale = serverMain.kinectGrabber.posY - 0.6;
		}
		scale  = scale + 0.5;
		if(scale < 0.1) scale = 0.1;
		int midX = Config.MOVIE_WIDTH / 2;
		int midY = Config.MOVIE_HEIGHT / 2;
		int sx1 = (int)(midX * (1. - scale));
		int sx2 = (int)(midX * (1. + scale));
		int sy1 = (int)(midY * (1. - scale));
		int sy2 = (int)(midY * (1. + scale));
		//prepare the image and tell serverMain
		mixedFrame.getGraphics().drawImage(mixedFrameTmp, 0, 0, Config.MOVIE_WIDTH, Config.MOVIE_HEIGHT, sx1, sy1, sx2, sy2, null);
		byte[] compressedFrame = Util.getCompressedFrame(mixedFrame);
		serverMain.setCurrentFrame(compressedFrame);
	}
	
	int fullWrap(int x, int range) {
		assert range > 0;
		while(x < 0) {
			x += range;
		} 
		return x % range;
	}
	
	void incrementDelayTimesMode() {
		delayTimesMode = nextDelayTimesMode;
		nextDelayTimesMode = DelayTimes.DelayTimesMode.values()[(nextDelayTimesMode.ordinal() + 1) % DelayTimes.DelayTimesMode.values().length];
	}
	
	void resetDelayTimes() {
		Arrays.fill(serverMain.movieMaker.delayTimesFrames, 0);
	}
	
	////////////////////////////////////////////////
	////////////////////////////////////////////////
	//test images
	////////////////////////////////////////////////
	
	int scalex = 0;
	int scaley = 50;
	BufferedImage temp = Util.createImageBuffer();

	void testImage() {
		//test pattern
		scalex = (scalex + 10) % 2550;
		scaley = (scaley + 1) % 2550;
		for(int i = 0; i < Config.MOVIE_WIDTH; i++) {
			for(int j = 0; j < Config.MOVIE_HEIGHT; j++) {
				int greyval = j * (scalex + 10) + i * (scaley + 10);
				int rgb = greyval;
				rgb = (rgb << 8) + greyval;
				rgb = (rgb << 8) + greyval;
				temp.setRGB(i, j, rgb);
			}
		}
		serverMain.setCurrentFrame(Util.getCompressedFrame(temp));
	}
	
	void testImage2() {
		//test pattern
		for(int i = 0; i < Config.MOVIE_WIDTH; i++) {
			for(int j = 0; j < Config.MOVIE_HEIGHT; j++) {
				int greyval = (j < (Config.MOVIE_HEIGHT / 2) && (serverMain.time / 10) % 2 == 0) ? 0 : 255;
				int rgb = greyval;
				rgb = (rgb << 8) + greyval;
				rgb = (rgb << 8) + greyval;
				temp.setRGB(i, j, rgb);
			}
		}
		serverMain.setCurrentFrame(Util.getCompressedFrame(temp));
	}
	
}
