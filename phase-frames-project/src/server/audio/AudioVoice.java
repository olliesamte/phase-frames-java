package server.audio;

import common.Config;

import net.beadsproject.beads.core.AudioContext;
import net.beadsproject.beads.core.UGen;
import net.beadsproject.beads.data.Sample;
import net.beadsproject.beads.ugens.Envelope;
import net.beadsproject.beads.ugens.Gain;
import net.beadsproject.beads.ugens.Glide;
import net.beadsproject.beads.ugens.GranularSamplePlayer;
import net.beadsproject.beads.ugens.Panner;
import net.beadsproject.beads.ugens.SamplePlayer;
import net.beadsproject.beads.ugens.TapIn;
import net.beadsproject.beads.ugens.TapOut;
import server.ServerMain;

public class AudioVoice {

	final int id;
	final AudioContext ac;
	final Gain output;
	final Gain gspGain; //
	
	float maxGain, pan;
	
	public Envelope outputEnv;
	Envelope gspGainEnv;
	GranularSamplePlayer gsp;
	Glide delay;
	Panner panner;
	Counter counter;
	TapIn tinClock, tinAudio;
	TapOut toutClock, toutAudio;
	
	public AudioVoice(AudioContext _ac, int _id) {
		this.id = _id;
		this.ac = _ac;
		//determine the gain based on this device's position
		maxGain = 0.5f;
		if(!ServerMain.ONE_AUDIO_VOICE) {
			//standard setup, fade out at edges
			int cutoff = 7;
			if(id < cutoff) {
				maxGain *= (float)Math.pow(0.01f + 0.99f * (id / (float)cutoff), 3f);
			} else if(id > (Config.numClients() - 1 - cutoff)) {
				int reverseID = Config.numClients() - 1 - id;
				maxGain *= (float)Math.pow(0.01f + 0.99f * (reverseID / (float)cutoff), 3f);
			}
			if(id == 8) {
				maxGain = 1;
			} 
			Config.stdout.println("Setting level for AudioVoice " + id + " to " + maxGain);
		} else {
			//debug one voice only
			if(id != 9) { maxGain = 0;}
		}
		//determine the pan
		pan = (float)Math.tanh((id - 8.5) * 0.3);
		//set up the main output stuff
		outputEnv = new Envelope(ac, maxGain);
		output = new Gain(ac, 2, outputEnv);
		panner = new Panner(ac, pan);
		output.addInput(panner);
		
		//set up the delay
		delay = new Glide(ac, 5);
		
		//the audio delay
		tinAudio = new TapIn(ac, 20000);
		toutAudio = new TapOut(ac, tinAudio, delay);
		panner.addInput(toutAudio);
		

		//set up the sample player (this does not go through the delay)
		counter = new Counter(ac);
		tinClock = new TapIn(ac, 20000);
		toutClock = new TapOut(ac, tinClock, delay);
		tinClock.addInput(counter);
		gsp = new GranularSamplePlayer(ac, 1);
		gsp.setPosition(toutClock);
		gsp.getGrainIntervalUGen().setValue(50);
		gsp.getGrainSizeUGen().setValue(80);
		gspGainEnv = new Envelope(ac, 0);
		gspGain = new Gain(ac, 1, gspGainEnv);
		gspGain.addInput(gsp);
		panner.addInput(gspGain);
	}
	
	public void updateDelayTime(double delayMS) {
		delay.setValue((float)delayMS);
	}
	
	public void setSample(Sample s) {
		gsp.setSample(s);
	}
	
	public void oneShot(Sample s, float rate) {
		SamplePlayer sp = new SamplePlayer(ac, s);
		sp.getRateUGen().setValue(rate);
		tinAudio.addInput(sp);
	}
	
	public UGen getOutput() {
		return output;
	}
	
	public void start(double pos) {
		counter.setPositionMS(pos);
		gspGainEnv.addSegment(1, 300);
	}
	
	public void stop() {
		gspGainEnv.addSegment(0, 700); 
	}

	public void setPlaybackRate(double playbackRate) {
		gsp.getRateUGen().setValue((float)playbackRate);
	}
	
}
