package server.audio;

import net.beadsproject.beads.core.AudioContext;
import net.beadsproject.beads.core.UGen;

public class Counter extends UGen {
	
	/*
	 * just counts milliseconds
	 */
	
	private double time = 0;
	private double msPerSample;
	private double msPerFrame;
	
	public Counter(AudioContext ac) {
		super(ac, 0, 1);
		msPerSample = ac.samplesToMs(1);
		msPerFrame = ac.samplesToMs(bufferSize);
	}
	
	public void reset() {
		time = 0;
	}
	
	public void setPositionMS(double d) {
		time = d;
	}

	@Override
	public void calculateBuffer() {
		for(int i = 0; i < bufferSize; i++) {
			bufOut[0][i] = (float)(time + i * msPerSample);
		}
		time += msPerFrame;
	}

}
