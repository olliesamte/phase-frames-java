package server.audio;

import net.beadsproject.beads.data.Sample;
import net.beadsproject.beads.data.SampleManager;

import common.Config;

public abstract class Audio {

	public static Sample projectorSound;
	public static Sample singleClick;
	
	public static void loadAudio() {
		projectorSound = SampleManager.sample(Config.MEDIA_DIR + "/audio/projector.wav");
		singleClick = SampleManager.sample(Config.MEDIA_DIR + "/audio/one_click.wav");
		
		for(int i = 0; i < Config.MOVIE_FILENAMES.length; i++) {
			SampleManager.sample(Config.MEDIA_DIR + "/audio/" + Config.MOVIE_FILENAMES[i] + ".wav");
		}
		
		for(int i = 1; i <= 10; i++) {
			SampleManager.sample(Config.MEDIA_DIR + "/audio/guit" + i + ".wav");
		}
		
	}
	
}
