package server;

import java.net.DatagramSocket;
import java.net.SocketException;

import common.Config;
import common.Util;

public class MessageSender {

	/*
	 * Broadcasts a single UDP message over multicast to all clients with the time.
	 * Clients set their system clocks accordingly.
	 * 
	 * Also can be used to broadcast control messages.
	 */
	
	DatagramSocket ds;
	ServerMain serverMain;
	
	public MessageSender(ServerMain _serverMain) {
		this.serverMain = _serverMain;
		try {
			ds = new DatagramSocket();
		} catch (SocketException e) {
			e.printStackTrace();
		}
		
		new Thread() {
			public void run() {
				while(true) {
					//send a multicast ping containing just the immediate time of the ping
//					long timeNow = System.currentTimeMillis(); 
//					Config.stdout.println("Sending timeNow=" + timeNow);
//					byte[] packet = Util.long2Bytes(timeNow);
					byte[] packet = Util.int2Bytes(serverMain.time);
					Util.sendMulticastPacket(ds, packet, Config.MULTICAST_PING_PORT);
					try {
						Thread.sleep(Config.PING_INTERVAL);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}.start();
		
	}
	
	public void message(String msg, int port) {
		byte[] packet = Util.string2Bytes(msg);
		String newMsg = Util.bytes2String(packet);
		Util.sendMulticastPacket(ds, packet, port);
	}
	
	public void messageCPP(String msg) {
		message(msg, Config.MULTICAST_CONTROL_PORT_CPP);
	}
	
	public void messageJava(String msg) {
		message(msg, Config.MULTICAST_CONTROL_PORT_JAVA);
	}
	
}
