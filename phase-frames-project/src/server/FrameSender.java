package server;

import java.net.DatagramSocket;
import java.net.SocketException;

import common.Config;
import common.Util;

public class FrameSender {
	
	//
	public final static boolean verbose = false;

	// This is our object that sends UDP out
	DatagramSocket ds; 

	//frame counter
	int frameNumber = 0;

	//time logging storage
	long lastCaptureTime = 0;
	long lastDrawTime = 0;

	public FrameSender() {
		// Setting up the DatagramSocket
		try {
			ds = new DatagramSocket();
		} catch (SocketException e) {
			e.printStackTrace(Config.stderr);
		}
	}
	
	//sends the ready compressed frame
	void sendFrame(byte[] fullData, int frame) {
		long time = System.currentTimeMillis();
		// Split into chunks
		int total = fullData.length;
		int current = 0;
		byte[] frameNumberBytes = Util.int2Bytes(frame);
		int totalPackets = (int) Math.ceil((double)fullData.length / Config.MAX_BUFFER_SIZE);
		byte[] totalPacketsBytes = Util.int2Bytes(totalPackets);
		if(verbose) Config.stdout.println("Sending frame : " + frame + " in : " + totalPackets + " packets, total size : " + total);
		time = System.currentTimeMillis();
		for(int i = 0; i < totalPackets; i++) {
			int diff = total - current;
			// we add 3 values at the start for frame number, part number and total parts for this frame
			// int for frame numer, int for part number and int for total parts.
			// int is 4 bytes (32 bit), 
			// so first 12 bytes are our data.
			int newBufferSize = (diff < Config.MAX_BUFFER_SIZE ? diff : Config.MAX_BUFFER_SIZE) + Config.EXTRA_BUFFER_DATA_SIZE;
			byte[] packet = new byte[newBufferSize];
			for(int x = 0; x < frameNumberBytes.length; x++) {
				packet[x] = frameNumberBytes[x];
			}
			byte[] packetNumberBytes = Util.int2Bytes(i);
			for(int x = 0; x < packetNumberBytes.length; x++) {
				packet[x + 4] = packetNumberBytes[x]; 
			}
			for(int x = 0; x < totalPacketsBytes.length; x++) {
				packet[x + 8] = totalPacketsBytes[x];
			}
			for(int x = 0; x < newBufferSize - Config.EXTRA_BUFFER_DATA_SIZE; x++) {
				packet[x + Config.EXTRA_BUFFER_DATA_SIZE] = fullData[(i * Config.MAX_BUFFER_SIZE) + x];
			}
			Util.sendMulticastPacket(ds, packet, Config.MULTICAST_STREAM_PORT);
			current += newBufferSize - Config.EXTRA_BUFFER_DATA_SIZE;
			if(verbose) System.out.println("Preparing packet: " + i + "/" + totalPackets + ", size of packet=" + newBufferSize);
		}
//		if(verbose) Config.stdout.println("Send timer : " + (System.currentTimeMillis() - time));
	}

}
