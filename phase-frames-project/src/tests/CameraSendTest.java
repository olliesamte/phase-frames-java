package tests;

import java.io.IOException;

import common.Config;

public class CameraSendTest {

	public static void main(String[] args) {
			int port = 10000;
			String[] commandarray = new String[] {"/home/pi/runvideo.sh", "" + port};
			try {
				Runtime.getRuntime().exec(commandarray);
			} catch (IOException e) {
				e.printStackTrace(Config.stderr);
			}
			Config.stdout.println("Starting writing camera video stream with command: " + commandarray[0] + ", on port " + port);
			//keep going
			while(true) {}
	
	}

}
