package tests;

import org.openkinect.processing.Kinect;

import processing.core.PApplet;
import processing.core.PVector;

public class KinectTest {
	
	// We'll use a lookup table so that we don't have to repeat the math over and over
	float[] depthLookUp = new float[2048];
	
	// Size of kinect image
	int w = 640;
	int h = 480;
	
	public static void main(String[] args) {
		new KinectTest();
	}
	
	public KinectTest() {
		PApplet applet = new PApplet();
		Kinect kinect = new Kinect(applet);
		kinect.start();
		kinect.enableDepth(true);
		kinect.processDepthImage(false);
		
		  // Lookup table for all possible depth values (0 - 2047)
		  for (int i = 0; i < depthLookUp.length; i++) {
		    depthLookUp[i] = rawDepthToMeters(i);
		  }
		  
		  while(true) {
			  
			  
			  // Get the raw depth as array of integers
			  int[] depth = kinect.getRawDepth();
			  
			// We're just going to calculate and draw every 4th pixel (equivalent of 160x120)
			  int skip = 4;
			  
			  //find the x position at the midline where the raw depth is lowest

			  int y = h / 2;
			  float lowest = Float.MAX_VALUE;
			  float xAtLowest = 0;
			  
			  for(int x=0; x<w; x+=skip) {
//			    for(int y=0; y<h; y+=skip) {
			      int offset = x+y*w;

			      // Convert kinect data to world xyz coordinate
			      int rawDepth = depth[offset];
			      
			      PVector v = depthToWorld(x,y,rawDepth);

//			      System.out.println(v.x + " " + v.y + " " + v.z);
			      
			      if(y > 220 && y < 260) {// && x > 310 && x < 330) {
			          if(rawDepth < lowest) {
			             lowest = rawDepth; 
			             xAtLowest = x;
			          }
			      }
			      
//			    }
			  }

			  System.out.println(xAtLowest + " " + lowest);

			  
			  try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		  }
	}
	
	// These functions come from: http://graphics.stanford.edu/~mdfisher/Kinect.html
	float rawDepthToMeters(int depthValue) {
	  if (depthValue < 2047) {
	    return (float)(1.0 / ((double)(depthValue) * -0.0030711016 + 3.3309495161));
	  }
	  return 0.0f;
	}
	
	PVector depthToWorld(int x, int y, int depthValue) {

		  final double fx_d = 1.0 / 5.9421434211923247e+02;
		  final double fy_d = 1.0 / 5.9104053696870778e+02;
		  final double cx_d = 3.3930780975300314e+02;
		  final double cy_d = 2.4273913761751615e+02;

		  PVector result = new PVector();
		  double depth =  depthLookUp[depthValue];//rawDepthToMeters(depthValue);
		  result.x = (float)((x - cx_d) * depth * fx_d);
		  result.y = (float)((y - cy_d) * depth * fy_d);
		  result.z = (float)(depth);
		  return result;
		}
	
}
