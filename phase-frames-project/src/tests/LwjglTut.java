//package tests;
//
//import org.lwjgl.BufferUtils;
//import org.lwjgl.Sys;
//
//import static org.lwjgl.glfw.GLFW.*;
//import static org.lwjgl.opengl.GL11.*;
//import static org.lwjgl.opengl.GL12.*;
//import static org.lwjgl.system.MemoryUtil.*; // NULL
//
//import java.awt.image.BufferedImage;
//import java.io.File;
//import java.io.IOException;
//import java.nio.ByteBuffer;
//
//import javax.imageio.ImageIO;
//
//import org.lwjgl.glfw.GLFWKeyCallback;
//import org.lwjgl.glfw.GLFWvidmode;
//import org.lwjgl.opengl.GLContext;
//import org.newdawn.slick.opengl.GLUtils;
//
//import sun.reflect.ReflectionFactory.GetReflectionFactoryAction;
//import common.Config;
//import common.Util;
//
//public class LwjglTut {
//	private static final int BYTES_PER_PIXEL = 4;
//
//	public boolean running = true;
//
//	public Long window;
//
//	public float m_Size = 1f;
//
//	private GLFWKeyCallback keyCallback;
//
//	BufferedImage image = null;
//	boolean imageLoaded = false;
//	public static void main(String[] args) {
//		System.out.println("LWJGL Version " + Sys.getVersion() + "is working.");
//		LwjglTut tut = new LwjglTut();
//		tut.start();
//	}
//
//	public void start() {
//		running = true;
//		run();
//	}
//
//	public void init() {
//		if (glfwInit() != GL_TRUE) {
//			System.err.println("GLFW init failed!");
//		}
//		
//		//glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
//
//		ByteBuffer vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
//		window = glfwCreateWindow(GLFWvidmode.width(vidmode), GLFWvidmode.height(vidmode), "Things", glfwGetPrimaryMonitor(), NULL);
//
//		if (window == NULL) {
//			System.err.println("Couldn't create our window");
//		}
//
//		
//
//		//glfwSetWindowPos(window, 100, 100);
//
//		glfwMakeContextCurrent(window);
//
//		// Enable VSYNC
//		glfwSwapInterval(1);
//
//		glfwShowWindow(window);
//
//		GLContext.createFromCurrent();
//
//		glClearColor(0.5f, 0.32f, 0.4f, 1.0f);
//		glEnable(GL_DEPTH_TEST);
//
//		System.out.println("OpenGL: " + glGetString(GL_VERSION));
//
//		glfwSetKeyCallback(window, keyCallback = new Input());
//		
//		
//		//image = new BufferedImage(1024, 1024, BufferedImage.TYPE_INT_RGB);
//
////		try {
////			image = //ImageIO.read(new File("../misc/Optimized-test.jpg"));
////			imageLoaded = true;
////		} catch (Exception e) {
////			e.printStackTrace();
////		}
//		
//		
////				scalex = (scalex + 10) % 2550;
////				scaley = (scaley + 1) % 2550;
////				for(int i = 0; i < Config.MOVIE_WIDTH; i++) {
////					for(int j = 0; j < Config.MOVIE_HEIGHT; j++) {
////						int greyval = j * (scalex + 10) + i * (scaley + 10);
////						int rgb = greyval;
////						rgb = (rgb << 8) + greyval;
////						rgb = (rgb << 8) + greyval;
////						image.setRGB(i, j, rgb);
////					}
////				}
//		
//	}
//	
//	boolean isShrinking = true;
//
//	public void update(float delta) {
//		glfwPollEvents();
//
//		 float changeSpeed = 0.5f;
//		 if(Input.keys[GLFW_KEY_DOWN]) {
//		
//			 m_Size -= changeSpeed * delta;
//		 }
//		
//		 if(Input.keys[GLFW_KEY_UP]) {
//			 m_Size += changeSpeed * delta;
//		 }
//		 
//		 if(Input.keys[GLFW_KEY_ESCAPE]) {
//			 running = false;
//		 }
//		 
//		 if(isShrinking) {
//			 m_Size -= changeSpeed * delta;
//			 if(m_Size <= 0f) {
//				 m_Size = 0f;
//				 isShrinking = false;
//			 }
//		 } else {
//			 m_Size += changeSpeed * delta;
//			 if(m_Size >= 1f) {
//				 m_Size = 1f;
//				 isShrinking = true;
//			 }
//		 }
//		
//		 System.out.println("Size : " + m_Size);
//	}
//
//	private static int loadTexture(BufferedImage image) {
//		int[] pixels = new int[image.getWidth() * image.getHeight()];
//		image.getRGB(0, 0, image.getWidth(), image.getHeight(), pixels, 0,
//				image.getWidth());
//		ByteBuffer buffer = BufferUtils.createByteBuffer(image.getWidth()
//				* image.getHeight() * BYTES_PER_PIXEL); // 4 for RGBA, 3 for RGB
//		for (int y = 0; y < image.getHeight(); y++) {
//			for (int x = 0; x < image.getWidth(); x++) {
//				int pixel = pixels[y * image.getWidth() + x];
//				buffer.put((byte) ((pixel >> 16) & 0xFF)); // Red component
//				buffer.put((byte) ((pixel >> 8) & 0xFF)); // Green component
//				buffer.put((byte) (pixel & 0xFF)); // Blue component
//				buffer.put((byte) ((pixel >> 24) & 0xFF)); // Alpha component.
//															// Only for RGBA
//			}
//		}
//		buffer.flip(); // FOR THE LOVE OF GOD DO NOT FORGET THIS
//		// You now have a ByteBuffer filled with the color data of each pixel.
//		// Now just create a texture ID and bind it. Then you can newTexture it
//		// using
//		// whatever OpenGL method you want, for example:
//		int textureID = glGenTextures(); // Generate texture ID
//		glBindTexture(GL_TEXTURE_2D, textureID); // Bind texture ID
//		// Setup wrap mode
//		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
//		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
//		// Setup texture scaling filtering
//		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
//				GL_LINEAR);
//		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
//				GL_LINEAR);
//		// Send texel data to OpenGL
//		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8,
//				image.getWidth(), image.getHeight(), 0, GL_RGBA,
//				GL_UNSIGNED_BYTE, buffer);
//		// Return the texture ID so we can bind it later again
//		return textureID;
//	}
//
//	//test pattern
//	int scalex = 0;
//	int scaley = 50;
//			
//	public void render() {
//		glfwSwapBuffers(window);
//
//		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//
////		glMatrixMode(GL_PROJECTION);
////		glLoadIdentity();
////		glTranslatef(0.0f, 0.0f, 0.0f);
////
////		glMatrixMode(GL_MODELVIEW);
////
////		
////		if(image != null && imageLoaded) {
////			int texture = loadTexture(image);
////			glBindTexture(GL_TEXTURE_2D, texture);
////		}
////		
////
////		glBegin(GL_QUADS);
////		glTexCoord2f(0f, 0f);
////		glVertex3f(-m_Size, m_Size, 0.0f);
////		glTexCoord2f(1f, 0f);
////		glVertex3f(m_Size, m_Size, 0.0f);
////		glTexCoord2f(1f, 1f);
////		glVertex3f(m_Size, -m_Size, 0.0f);
////		glTexCoord2f(0f, 1f);
////		glVertex3f(-m_Size, -m_Size, 0.0f);
////		glEnd();
//	}
//
//	public void run() {
//		init();
//		double lastLoopTime = glfwGetTime();
//		while (running) {
//			double time = glfwGetTime();
//			float delta = (float) (time - lastLoopTime);
//			lastLoopTime = time;
//			update(delta);
//			render();
//
//			if (glfwWindowShouldClose(window) == GL_TRUE) {
//				running = false;
//			}
//		}
//		keyCallback.release();
//	}
//}
