package tests;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.JFrame;


public class PiCameraSenderTest {
	
	static int W = 640;
	static int H = 480;
	static int FPS = 15;
	
	JFrame swingWindow;

	public static void main(String[] args) {
		PiCameraSenderTest sender = new PiCameraSenderTest();
		new Thread() {
			public void run() {
				while(true) {
					sender.sendACameraStill();
					try {
						Thread.sleep((int)(1000. / FPS));
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}.start();
	}
	
	public PiCameraSenderTest() {
		swingWindow = new JFrame("Cam Test");
		swingWindow.setVisible(true);
//		GraphicsDevice device = GraphicsEnvironment
//				.getLocalGraphicsEnvironment().getScreenDevices()[0];
//		device.setFullScreenWindow(swingWindow);
		swingWindow.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					System.exit(0);
				}
			}
		});
		swingWindow.setSize(W, H);
	}
	
	
	public void sendACameraStill() {
//		new Thread() {
//			public void run() {
				try {
					System.out.println("Trying to send a picture");
					long time = System.currentTimeMillis();
					Process p = Runtime.getRuntime().exec(
							"raspistill -w 640 -h 480 -n -th none -t 1 -e jpg -ex night -awb off -cfx 128:128 -o -"
							);
					System.out.println("Time to start process: " + (System.currentTimeMillis() - time) + "ms");
					time = System.currentTimeMillis();
					InputStream is = p.getInputStream();
					System.out.println("Time to get input stream: " + (System.currentTimeMillis() - time) + "ms");
					time = System.currentTimeMillis();
					BufferedImage bufImg = ImageIO.read(is);
					System.out.println("Time to create img: " + (System.currentTimeMillis() - time) + "ms");
					time = System.currentTimeMillis();
					swingWindow.getGraphics().clearRect(0, 0, W, H);
					swingWindow.getGraphics().drawImage(bufImg, 0, 0, W, H, null);
					System.out.println("Time to draw: " + (System.currentTimeMillis() - time) + "ms");
					time = System.currentTimeMillis();
					//print info
					System.out.println("Image info: ");
					System.out.println("--- height: " + bufImg.getHeight());
					System.out.println("--- width : " + bufImg.getWidth());
					System.out.println("--- type  : " + bufImg.getType());
				} catch (Exception err) {
					err.printStackTrace();
				}
//			}
//		}.start();
	}
	
}
