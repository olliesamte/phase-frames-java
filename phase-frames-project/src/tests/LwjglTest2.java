//package tests;
//
//import static org.lwjgl.glfw.Callbacks.errorCallbackPrint;
//import static org.lwjgl.glfw.GLFW.GLFW_KEY_ESCAPE;
//import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;
//import static org.lwjgl.glfw.GLFW.GLFW_RESIZABLE;
//import static org.lwjgl.glfw.GLFW.GLFW_VISIBLE;
//import static org.lwjgl.glfw.GLFW.glfwCreateWindow;
//import static org.lwjgl.glfw.GLFW.glfwDefaultWindowHints;
//import static org.lwjgl.glfw.GLFW.glfwDestroyWindow;
//import static org.lwjgl.glfw.GLFW.glfwGetPrimaryMonitor;
//import static org.lwjgl.glfw.GLFW.glfwGetVideoMode;
//import static org.lwjgl.glfw.GLFW.glfwInit;
//import static org.lwjgl.glfw.GLFW.glfwMakeContextCurrent;
//import static org.lwjgl.glfw.GLFW.glfwPollEvents;
//import static org.lwjgl.glfw.GLFW.glfwSetErrorCallback;
//import static org.lwjgl.glfw.GLFW.glfwSetKeyCallback;
//import static org.lwjgl.glfw.GLFW.glfwSetWindowPos;
//import static org.lwjgl.glfw.GLFW.glfwSetWindowShouldClose;
//import static org.lwjgl.glfw.GLFW.glfwShowWindow;
//import static org.lwjgl.glfw.GLFW.glfwSwapInterval;
//import static org.lwjgl.glfw.GLFW.glfwTerminate;
//import static org.lwjgl.glfw.GLFW.glfwWindowHint;
//import static org.lwjgl.glfw.GLFW.glfwWindowShouldClose;
//import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
//import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
//import static org.lwjgl.opengl.GL11.GL_FALSE;
//import static org.lwjgl.opengl.GL11.GL_LINEAR;
//import static org.lwjgl.opengl.GL11.GL_RGBA;
//import static org.lwjgl.opengl.GL11.GL_RGBA8;
//import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
//import static org.lwjgl.opengl.GL11.GL_TEXTURE_MAG_FILTER;
//import static org.lwjgl.opengl.GL11.GL_TEXTURE_MIN_FILTER;
//import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_S;
//import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_T;
//import static org.lwjgl.opengl.GL11.GL_TRUE;
//import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
//import static org.lwjgl.opengl.GL11.glBindTexture;
//import static org.lwjgl.opengl.GL11.glClear;
//import static org.lwjgl.opengl.GL11.glClearColor;
//import static org.lwjgl.opengl.GL11.glGenTextures;
//import static org.lwjgl.opengl.GL11.glTexImage2D;
//import static org.lwjgl.opengl.GL11.glTexParameteri;
//import static org.lwjgl.system.MemoryUtil.NULL;
//
//import java.awt.image.BufferedImage;
//import java.io.File;
//import java.io.IOException;
//import java.nio.ByteBuffer;
//
//import javax.imageio.ImageIO;
//
//import org.lwjgl.BufferUtils;
//import org.lwjgl.Sys;
//import org.lwjgl.glfw.GLFW;
//import org.lwjgl.glfw.GLFWErrorCallback;
//import org.lwjgl.glfw.GLFWKeyCallback;
//import org.lwjgl.glfw.GLFWvidmode;
//import org.lwjgl.opengl.GL11;
//import org.lwjgl.opengl.GL12;
//import org.lwjgl.opengl.GLContext;
//
//import common.Util;
// 
//public class LwjglTest2 {
// 
//    // We need to strongly reference callback instances.
//    private GLFWErrorCallback errorCallback;
//    private GLFWKeyCallback   keyCallback;
// 
//    // The window handle
//    private long window;
// 
//    public void run() {
//        System.out.println("Hello LWJGL " + Sys.getVersion() + "!");
// 
//        try {
//            init();
//            loop();
// 
//            // Release window and window callbacks
//            glfwDestroyWindow(window);
//            keyCallback.release();
//        } finally {
//            // Terminate GLFW and release the GLFWerrorfun
//            glfwTerminate();
//            errorCallback.release();
//        }
//    }
// 
//    private void init() {
//        // Setup an error callback. The default implementation
//        // will print the error message in System.err.
//        glfwSetErrorCallback(errorCallback = errorCallbackPrint(System.err));
// 
//        // Initialize GLFW. Most GLFW functions will not work before doing this.
//        if ( glfwInit() != GL11.GL_TRUE )
//            throw new IllegalStateException("Unable to initialize GLFW");
// 
//        // Configure our window
//        glfwDefaultWindowHints(); // optional, the current window hints are already the default
//        glfwWindowHint(GLFW_VISIBLE, GL_FALSE); // the window will stay hidden after creation
//        glfwWindowHint(GLFW_RESIZABLE, GL_TRUE); // the window will be resizable
// 
//        int WIDTH = 300;
//        int HEIGHT = 300;
// 
//        // Create the window
//        window = glfwCreateWindow(WIDTH, HEIGHT, "Hello World!", NULL, NULL);
//        if ( window == NULL )
//            throw new RuntimeException("Failed to create the GLFW window");
// 
//        // Setup a key callback. It will be called every time a key is pressed, repeated or released.
//        glfwSetKeyCallback(window, keyCallback = new GLFWKeyCallback() {
//            @Override
//            public void invoke(long window, int key, int scancode, int action, int mods) {
//                if ( key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE )
//                    glfwSetWindowShouldClose(window, GL_TRUE); // We will detect this in our rendering loop
//            }
//        });
// 
//        // Get the resolution of the primary monitor
//        ByteBuffer vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
//        // Center our window
//        glfwSetWindowPos(
//            window,
//            (GLFWvidmode.width(vidmode) - WIDTH) / 2,
//            (GLFWvidmode.height(vidmode) - HEIGHT) / 2
//        );
// 
//        // Make the OpenGL context current
//        glfwMakeContextCurrent(window);
//        // Enable v-sync
//        glfwSwapInterval(1);
// 
//        // Make the window visible
//        glfwShowWindow(window);
//        
//        
//        
//        
//        
//    }
// 
//    private void loop() {
//        // This line is critical for LWJGL's interoperation with GLFW's
//        // OpenGL context, or any context that is managed externally.
//        // LWJGL detects the context that is current in the current thread,
//        // creates the ContextCapabilities instance and makes the OpenGL
//        // bindings available for use.
//        GLContext.createFromCurrent();
// 
//        // Set the clear color
//        glClearColor(1.0f, 0.0f, 0.0f, 0.0f);
// 
//        // Run the rendering loop until the user has attempted to close
//        // the window or has pressed the ESCAPE key.
//        while ( glfwWindowShouldClose(window) == GL_FALSE ) {
//            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the framebuffer
//
////            byte[] bytes = new byte[100];
////            glTexImage2D(GL_TEXTURE_2D, 1, GL_RGB, Config.SCREEN_WIDTH, Config.SCREEN_WIDTH, 0, GL_RGB, GL_INT, ByteBuffer.wrap(bytes));
//            
//            
//            BufferedImage image = Util.createImageBuffer();
//            
//            try {
//				image = ImageIO.read(new File("../misc/test.jpg"));
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
// 
//            
//            int[] pixels = new int[image.getWidth() * image.getHeight()];
//            image.getRGB(0, 0, image.getWidth(), image.getHeight(), pixels, 0, image.getWidth());
//    
//            ByteBuffer buffer = BufferUtils.createByteBuffer(image.getWidth() * image.getHeight() * 3); //4 for RGBA, 3 for RGB
//            
//            for(int y = 0; y < image.getHeight(); y++){
//               for(int x = 0; x < image.getWidth(); x++){
//                   int pixel = pixels[y * image.getWidth() + x];
//                   buffer.put((byte) ((pixel >> 16) & 0xFF));     // Red component
//                   buffer.put((byte) ((pixel >> 8) & 0xFF));      // Green component
//                   buffer.put((byte) (pixel & 0xFF));               // Blue component
//                   buffer.put((byte) ((pixel >> 24) & 0xFF));    // Alpha component. Only for RGBA
//               }
//            }
//     
//            buffer.flip(); //FOR THE LOVE OF GOD DO NOT FORGET THIS
//     
//            // You now have a ByteBuffer filled with the color data of each pixel.
//            // Now just create a texture ID and bind it. Then you can load it using 
//            // whatever OpenGL method you want, for example:
//            int textureID = glGenTextures(); //Generate texture ID
//           glBindTexture(GL_TEXTURE_2D, textureID); //Bind texture ID
//            
//            //Setup wrap mode
//            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
//            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);
//    
//            //Setup texture scaling filtering
//            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//            
//            //Send texel data to OpenGL
//            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, image.getWidth(), image.getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
//            
//            
//            
//            
//            //draw a quad>>>>>
//         // Clear the screen and depth buffer
////            GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);	
//             
//            // set the color of the quad (R,G,B,A)
////            GL11.glColor3f(0.5f,0.5f,1.0f);
//             
//            // draw quad
//            
////            GL11.glBindTexture(GL11.GL_TEXTURE_2D, texture);
//            
//            GL11.glBegin(GL11.GL_QUADS);
//                GL11.glVertex2f(100,100);
//                GL11.glVertex2f(100+200,100+200);
//                GL11.glVertex2f(100+200,100);
//                GL11.glVertex2f(100,100+200);
//            GL11.glEnd();
//            
//            
//            
//          GLFW.glfwSwapBuffers(window); // swap the color buffers
//            
//            
//            // Poll for window events. The key callback above will only be
//            // invoked during this call.
//            glfwPollEvents();
//        }
//    }
// 
//    public static void main(String[] args) {
//        new LwjglTest2().run();
//    }
// 
//}