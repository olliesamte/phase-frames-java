package tests;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.net.InetSocketAddress;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import common.Util;

import uk.co.caprica.vlcj.component.DirectMediaPlayerComponent;
import uk.co.caprica.vlcj.discovery.NativeDiscovery;
import uk.co.caprica.vlcj.player.direct.BufferFormat;
import uk.co.caprica.vlcj.player.direct.BufferFormatCallback;
import uk.co.caprica.vlcj.player.direct.DirectMediaPlayer;
import uk.co.caprica.vlcj.player.direct.RenderCallback;
import uk.co.caprica.vlcj.player.direct.RenderCallbackAdapter;
import uk.co.caprica.vlcj.player.direct.format.RV32BufferFormat;

public class VLCJStreamTest2 {

    private static final int width = 600;
    private static final int height = 400;
    private  BufferedImage image;
    private final DirectMediaPlayerComponent mediaPlayerComponent;

    public static void main(final String[] args) {
    	
    	new JFrame().setVisible(true);
    	
        new NativeDiscovery().discover();
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new VLCJStreamTest2(args);
            }
        });
    }

    public VLCJStreamTest2(String[] args) {
//        frame = new JFrame("Direct Media Player");
//        frame.setBounds(100, 100, width, height);
//        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
//        frame.addWindowListener(new WindowAdapter() {
//            @Override
//            public void windowClosing(WindowEvent e) {
//                System.exit(0);
//            }
//        });
//        videoSurface = new VideoSurfacePanel();
//        frame.setContentPane(videoSurface);
//        image = GraphicsEnvironment
//            .getLocalGraphicsEnvironment()
//            .getDefaultScreenDevice()
//            .getDefaultConfiguration()
//            .createCompatibleImage(width, height);
        BufferFormatCallback bufferFormatCallback = new BufferFormatCallback() {
            @Override
            public BufferFormat getBufferFormat(int sourceWidth, int sourceHeight) {
                return new RV32BufferFormat(width, height);
            }
        };
        mediaPlayerComponent = new DirectMediaPlayerComponent(bufferFormatCallback) {
            @Override
            protected RenderCallback onGetRenderCallback() {
                return new TutorialRenderCallbackAdapter();
            }
        };

        String addr = Util.hostname2IP("b827eb658d29");
        mediaPlayerComponent.getMediaPlayer().playMedia("rtsp://" + addr + ":8554/");
    }

    private class TutorialRenderCallbackAdapter extends RenderCallbackAdapter {
        private TutorialRenderCallbackAdapter() {
            super(new int[width * height]);
        }

        @Override
        protected void onDisplay(DirectMediaPlayer mediaPlayer, int[] rgbBuffer) {
            // Simply copy buffer to the image and repaint
            image.setRGB(0, 0, width, height, rgbBuffer, 0, width);
            System.out.println("Got frame!");
        }
    }
}

