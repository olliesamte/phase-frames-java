package tests;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import client.ClientMain;

import common.Config;

public class CameraSenderOLD {
	
	//
	public static final boolean verbose = true;

	//the owner
	final ClientMain clientMain;

	//networking
	Socket networkSocket;
	int port;

	public CameraSenderOLD(ClientMain _clientMain) {
		this.clientMain = _clientMain;
		//work out the port to send on
		port = Config.CAMERA_STREAM_START_PORT + clientMain.getID();
		try {
			networkSocket = new Socket(Config.SERVER_HOSTNAME, port);
		} catch (Exception e) {
			e.printStackTrace(Config.stderr);
		}
		startCameraVideoStream();
	}

	public void startCameraVideoStream() {
		new Thread() {
			public void run() {
				try {
					Process p = Runtime.getRuntime().exec(
							//set to run for 10 hours = 36000000ms
							"raspivid -w " + Config.MOVIE_WIDTH + " -h " + Config.MOVIE_HEIGHT + " -t 36000000 -n -o -"
							);
					if(verbose) Config.stdout.println("Starting writing camera video stream...");
					BufferedInputStream bis = new BufferedInputStream(p.getInputStream());
					int read;
					while((read = bis.read()) != -1) {
						networkSocket.getOutputStream().write(read);
					}
					bis.close();
					if(verbose) Config.stdout.println("Done writing camera video stream.");
				}
				catch (IOException ieo) {
					ieo.printStackTrace(Config.stderr);
				}
			}
		}.start();
	}

	public void startStillsStream() {
		new Thread() {
			public void run() {
				while(true) {
					sendACameraStill();
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}.start();
	}

	//** incomplete - currently this takes a still and turns it into bytes **
	//doesn't send anywhere nor print to screen
	public void sendACameraStill() {
		try {
			// *** this code is out of date... see tests.PiCameraSenderTest. ****
			Process p = Runtime.getRuntime().exec("raspistill -w 640 -h 480 -n -th none -t 100 -e jpg -o -");
			BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
			ByteArrayOutputStream boss = new ByteArrayOutputStream();
			while (bri.read() != -1) {
				boss.write(bri.read());
			}
			byte[] bytes = boss.toByteArray();
			//temp code
			//			ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
			//			BufferedImage bufImg = ImageIO.read(bis);
			//			swingWindow.getGraphics().drawImage(bufImg, 0, 0, Config.SCREEN_WIDTH, Config.SCREEN_HEIGHT, null);
			//			clientMain.drawFullScreen(bufImg);

			bri.close();
		} catch (Exception err) {
			err.printStackTrace();
		}
	}

}
