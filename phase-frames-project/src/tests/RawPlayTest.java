package tests;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.JPanel;

import common.Config;
import common.MovieConverter;
import common.Util;

public class RawPlayTest {
	
	static BufferedImage bim;

	public static void main(String[] args) {
		
		byte[][] bytes = MovieConverter.read(Config.MEDIA_DIR + "/raw/test.mov");
		
		System.out.println("Frames: " + bytes.length + ", bytes per frame: " + bytes[100].length);
		
		bim = Util.getDecompressedFrame(bytes[100]);
		
		System.out.println("Width: " + bim.getWidth() + ", height: " + bim.getHeight());
		
		JFrame frame = new JFrame();
		frame.setVisible(true);
		frame.setMinimumSize(new Dimension(500,500));
		
		JPanel pane = new JPanel() {
			public void paintComponent(Graphics g) {
				g.drawImage(bim, 0, 0, null);
			}
		};
		frame.setContentPane(pane);
		
		new Thread() {
			public void run() {
				int t = 0;
				while(true) {
					bim = Util.getDecompressedFrame(bytes[t++]);
					pane.repaint();
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}.start();
	}
}
