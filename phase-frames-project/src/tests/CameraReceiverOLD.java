package tests;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

import uk.co.caprica.vlcj.player.MediaPlayer;
import common.Config;

public class CameraReceiverOLD {
	
	//
	public static final boolean verbose = true;
	
	//array of server sockets, one for each device
	ServerSocket[] sockets = new ServerSocket[Config.numClients()];

	public CameraReceiverOLD() {
		//set up the sockets
		for(int i = 0; i < Config.numClients(); i++) {
			try {
				sockets[i] = new ServerSocket(Config.CAMERA_STREAM_START_PORT + i);
			} catch (IOException e) {
				e.printStackTrace(Config.stderr);
			}
		}
		//set up threads to poll for frames
		for(int i = 0; i < Config.numClients(); i++) {
			int ssid = i;
			new Thread() {
				public void run() {
					pollForPacket(ssid);	
				}
			}.start();
		}
	}
	
	
	
	

	void pollForPacket(int id) {
		try {
			if(verbose) Config.stdout.println("Polling for packet from client " + id);
			Socket s = sockets[id].accept();
			InputStream in = s.getInputStream();
			int bufferSize = s.getReceiveBufferSize();
			byte[] bytes = new byte[bufferSize];
			if(verbose) Config.stdout.println("Receiving from client " + id);
			int byteCount;
			while ((byteCount = in.read(bytes)) > 0) {
				
				//TODO
				
				
			}
			if(verbose) Config.stdout.println("Done receiving stream from client " + id);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
