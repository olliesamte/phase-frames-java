package tests;

import uk.co.caprica.vlcj.component.DirectMediaPlayerComponent;
import uk.co.caprica.vlcj.discovery.NativeDiscovery;
import uk.co.caprica.vlcj.player.direct.BufferFormat;
import uk.co.caprica.vlcj.player.direct.BufferFormatCallback;
import uk.co.caprica.vlcj.player.direct.DirectMediaPlayer;
import uk.co.caprica.vlcj.player.direct.RenderCallback;
import uk.co.caprica.vlcj.player.direct.RenderCallbackAdapter;
import uk.co.caprica.vlcj.player.direct.format.RV32BufferFormat;
import common.Config;
import common.Util;

//"b827eb1958ea",
//"b827eb0aa031",
//"b827eb3e923f",
//"b827eb5b3a47",
//"b827eb19eb64",
//"b827ebf4c035",
//"b827eb658d29",
//"b827ebd40add",
//"b827ebd3c42f",
//"b827ebdf8039",
//"b827ebc93d8d",
//"b827eb9d001a",
//"b827eb78be42",
//"b827eb155ae2",
//"b827eb9eb223",
//"b827ebb8ec53",
//"b827ebc6f4ae",
//"b827ebd77d1c",


public class TestForIncomingStream {


    private DirectMediaPlayerComponent mediaPlayerComponent;
    
    String address, hostname;
    int port;
    Thread theThread;
    
	public static void main(String[] args) {	
		new NativeDiscovery().discover();
//		String hostname = "b827eb0aa031";
//		new TestForIncomingStream(hostname, 10000);
		
		for(String hostname : Config.HOSTNAMES) {
	    	int id = Config.DEVICES.get(hostname);
	    	int port = Config.CAMERA_STREAM_START_PORT + id; 
			new TestForIncomingStream(hostname, port);
		}
		
	}

    public TestForIncomingStream(String hostname, int port) {
    	this.hostname = hostname;
    	this.address = Util.hostname2IP(hostname);   
    	theThread = new Thread() {
    		public void run() {
    			BufferFormatCallback bufferFormatCallback = new BufferFormatCallback() {
    	            @Override
    	            public BufferFormat getBufferFormat(int sourceWidth, int sourceHeight) {
    	                return new RV32BufferFormat(Config.MOVIE_WIDTH, Config.MOVIE_HEIGHT);
    	            }
    	        };
    	        mediaPlayerComponent = new DirectMediaPlayerComponent(bufferFormatCallback) {
    	            @Override
    	            protected RenderCallback onGetRenderCallback() {
    	                return new PhaseFramesRenderCallbackAdapter();
    	            }
    	        };
    	        Config.stdout.println("Streaming from address: " + address + ", port   : " + port);
    	        DirectMediaPlayer player = mediaPlayerComponent.getMediaPlayer();
    	        player.playMedia("rtsp://" + address + ":" + port + "/");
    	        player.pause();		//why is this here?
    		}
    	};
    	theThread.start(); 
    }

    private class PhaseFramesRenderCallbackAdapter extends RenderCallbackAdapter {
        private PhaseFramesRenderCallbackAdapter() {
            super(new int[Config.MOVIE_WIDTH * Config.MOVIE_HEIGHT]);
        }

        @Override
        protected void onDisplay(DirectMediaPlayer mediaPlayer, int[] rgbBuffer) {
//        	Config.stdout.println("Got frame from " + hostname + ":" + port);   
        }
    }
    
	

}
