package client;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;

import javax.smartcardio.CommandAPDU;
import javax.sql.CommonDataSource;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.input.Keyboard;

import common.Config;
import common.Util;

public class ClientOpenGLRenderer {

	BufferedImage m_NextImage = null;
	BufferedImage m_CurrentImage = null;
	float uv_x1 = 0f;
	float uv_y1 = 0f;
	float uv_x2 = 1f;
	float uv_y2 = 0f;
	float uv_x3 = 1f;
	float uv_y3 = 1f;
	float uv_x4 = 0f;
	float uv_y4 = 1f;

	public void start() {

		DisplayMode mode = null;

		try {
			DisplayMode[] modes = Display.getAvailableDisplayModes();
			int index = 0;
			int width = 0;
			for (int i = 0; i < modes.length; i++) {
				if (modes[i].getWidth() > width) {
					width = modes[i].getWidth();
					index = i;
				}
			}
			Config.stdout.println("Screen : " + modes[index].getWidth() + " : "
					+ modes[index].getHeight());
			mode = modes[index];
			Display.setDisplayMode(modes[index]);
			Display.setFullscreen(true);
			Display.setVSyncEnabled(false);
			Display.create();
		} catch (LWJGLException e) {
			e.printStackTrace();
			System.exit(0);
		}
		
		Config.stdout.println("Max Texture Size : " + GL11.glGetInteger(GL11.GL_MAX_TEXTURE_SIZE));

		// Init OpenGL
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glOrtho(0, mode.getWidth(), 0, mode.getHeight(), 1, -1);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);

		// Generate a small test image by drawing to a BufferedImage
		// It's of course also possible to just load an image using
		// ImageIO.load()
		BufferedImage test = new BufferedImage(128, 128,
				BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = test.createGraphics();

		g2d.setColor(new Color(1.0f, 1.0f, 1.0f, 0.5f));
		g2d.fillRect(0, 0, 128, 128); // A transparent white background

		g2d.setColor(Color.red);
		g2d.drawRect(0, 0, 127, 127); // A red frame around the image
		g2d.fillRect(10, 10, 10, 10); // A red box

		g2d.setColor(Color.blue);
		g2d.drawString("Test image", 10, 64); // Some blue text

		int textureID = loadTexture(test);

		GL11.glEnable(GL11.GL_TEXTURE_2D);

		while (!Display.isCloseRequested()) {

			long t = System.currentTimeMillis();
			
			float delta = getDelta();
			Config.stdout.println("Delta : " + delta);
			pollInput(delta);
			

			Config.stdout.println("Time in GL call 1: " + (System.currentTimeMillis() - t));
			t = System.currentTimeMillis();
			
			if(m_NextImage != null) {
				m_CurrentImage = m_NextImage;
				m_NextImage = null;
				textureID = loadTexture(m_CurrentImage);		
			}
			

			Config.stdout.println("Time in GL call 2: " + (System.currentTimeMillis() - t));
			t = System.currentTimeMillis();

			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);

			GL11.glColor3f(0.5f, 0.5f, 1.0f);

			// draw quad
			int width = mode.getWidth();
			int height = mode.getHeight();
			

			Config.stdout.println("Time in GL call 3: " + (System.currentTimeMillis() - t));
			t = System.currentTimeMillis();

//			GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureID);

			GL11.glBegin(GL11.GL_QUADS);
			GL11.glTexCoord2f(uv_x1, uv_y1);
			GL11.glVertex2f(0f, height);

			GL11.glTexCoord2f(uv_x2, uv_y2);
			GL11.glVertex2f(width, height);

			GL11.glTexCoord2f(uv_x3, uv_y3);
			GL11.glVertex2f(width, 0f);

			GL11.glTexCoord2f(uv_x4, uv_y4);
			GL11.glVertex2f(0f, 0f);
			GL11.glEnd();

			Config.stdout.println("Time in GL call 4: " + (System.currentTimeMillis() - t));
			t = System.currentTimeMillis();

			Display.update();
			

			Config.stdout.println("Time in GL call 5: " + (System.currentTimeMillis() - t));
			t = System.currentTimeMillis();
			

			
		}

		Display.destroy();
	}

	public void SetNextImage(BufferedImage nextImage) {
		Config.stdout.println("Got new image in OpenGLRenderer");
		m_NextImage = nextImage;
		
	}

	long lastFrame;

	public void pollInput(float delta) {

		while (Keyboard.next()) {
			if (Keyboard.getEventKey() == Keyboard.KEY_ESCAPE) {
				Util.shutdown();
			}
		}
	}

	public int getDelta() {
		long time = getTime();
		int delta = (int) (time - lastFrame);
		lastFrame = time;

		return delta;
	}

	/**
	 * Get the accurate system time
	 * 
	 * @return The system time in milliseconds
	 */
	public long getTime() {
		return (Sys.getTime() * 1000) / Sys.getTimerResolution();
	}

	private static final int BYTES_PER_PIXEL = 4;

	private int loadTexture(BufferedImage image) {
		int dim = 2;
		
		while(image.getWidth() > dim || image.getHeight() > dim) {
			dim *= 2;
		}
		
		Config.stdout.println("dims: " + dim + ", image w = " + image.getWidth() + ", image h = " + image.getHeight());
		int[] pixels = new int[image.getWidth() * image.getHeight()];
		image.getRGB(0, 0, image.getWidth(), image.getHeight(), pixels, 0,
				image.getWidth());

		ByteBuffer buffer = BufferUtils.createByteBuffer(dim
				* dim * BYTES_PER_PIXEL); // 4 for RGBA, 3 for RGB

		for (int y = 0; y < dim; y++) {
			for (int x = 0; x < dim; x++) {
				int pixel = 0;
				if(x <image.getWidth() && y < image.getHeight()) {
					pixel = pixels[y * image.getWidth() + x];
				}
				buffer.put((byte) ((pixel >> 16) & 0xFF)); // Red component
				buffer.put((byte) ((pixel >> 8) & 0xFF)); // Green component
				buffer.put((byte) (pixel & 0xFF)); // Blue component
				buffer.put((byte) ((pixel >> 24) & 0xFF)); // Alpha component.
															// Only for RGBA
			}
		}

		buffer.flip(); // FOR THE LOVE OF GOD DO NOT FORGET THIS

		// You now have a ByteBuffer filled with the color data of each pixel.
		// Now just create a texture ID and bind it. Then you can load it using
		// whatever OpenGL method you want, for example:

		int textureID = GL11.glGenTextures(); // Generate texture ID
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureID); // Bind texture ID

		// Setup wrap mode
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S,
				GL12.GL_CLAMP_TO_EDGE);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T,
				GL12.GL_CLAMP_TO_EDGE);

		// Setup texture scaling filtering
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER,
				GL11.GL_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER,
				GL11.GL_LINEAR);

		// Send texel data to OpenGL
		GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA8,
				dim, dim, 0, GL11.GL_RGBA,
				GL11.GL_UNSIGNED_BYTE, buffer);
		
		// Cache the new uvs
//		float widthRatio = image.getWidth() / dim;
//		float heightRatio = image.getHeight() / dim;
//		
//		uv_x2 = widthRatio;
//		
//		uv_x3 = widthRatio;
//		uv_y3 = heightRatio;
//		
//		uv_y4 = heightRatio;

		// Return the texture ID so we can bind it later again
		return textureID;
	}

	// public static void main(String[] argv) {
	// ClientOpenGLRenderer displayExample = new ClientOpenGLRenderer();
	// displayExample.start();
	// }
}
