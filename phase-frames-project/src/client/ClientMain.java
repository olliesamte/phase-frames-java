package client;
import java.awt.AWTException;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Point;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.MemoryImageSource;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.sun.java.swing.SwingUtilities3;

import common.Config;
import common.EZShell;
import common.Util;

public class ClientMain {
	
	//
	public final static boolean verbose = false;
	public final static boolean USE_GL = false;

	//id stuff
	String hostname;
	int id;
	
	//the graphics context
	JFrame swingWindow;
	MainPanel mainPanel;
	
	ClientOpenGLRenderer glRenderer;
	Thread glThread;
	
	//tool for mouse control - used to hide mouse (also possibly to keep screen awake!)
	Robot mouse;
	
	//the networking elements
	FrameReceiver videoReceiver;
	CameraSender cameraSender;
	
	//the multicast socket for listening for timing pings
	MulticastSocket pingSocket;
	byte[] pingMsg = new byte[8];
	long timeOffsetOfServer = 0;
	
	//the mutlicast socket for listening to other control messages
	MulticastSocket controlSocket;
	byte[] commandMsg = new byte[4000]; //arbitrary length!
	
	//debug stuff
	String statusText = "debug";
	
	BufferStrategy bufferStrategy;

	public static void main(String[] args) {
		if(args.length > 0 && args[0].equals("output_to_file")) {
			Config.setOutputToFile("/home/pi");
		}
		Config.stdout.println("Screen size detected is: " + Config.SCREEN_WIDTH + " x " + Config.SCREEN_HEIGHT);
		new ClientMain();
	}

	public ClientMain() {
		//get my index
		hostname = Util.findMyHostname();
		id = Util.findMyIndex();
		if(USE_GL) {
			glRenderer = new ClientOpenGLRenderer();
			glThread = new Thread(new Runnable() {
				
				@Override
				public void run() {
					glRenderer.start();
				}
			});
			glThread.start();
		}
		//set up the Swing window
		swingWindow = new JFrame("PhaseFrames");
		swingWindow.setSize(Config.SCREEN_WIDTH, Config.SCREEN_HEIGHT);
		swingWindow.setVisible(true);
		mainPanel = new MainPanel();
		mainPanel.setMinimumSize(new Dimension(Config.SCREEN_WIDTH, Config.SCREEN_HEIGHT));
		mainPanel.setMaximumSize(new Dimension(Config.SCREEN_WIDTH, Config.SCREEN_HEIGHT));
		mainPanel.setPreferredSize(new Dimension(Config.SCREEN_WIDTH, Config.SCREEN_HEIGHT));
		SwingUtilities3.setVsyncRequested(swingWindow, true);
		swingWindow.setContentPane(mainPanel);
		swingWindow.setIgnoreRepaint(true);
		swingWindow.createBufferStrategy(2);
		bufferStrategy = swingWindow.getBufferStrategy();
		Config.stdout.println("BS MB:" + bufferStrategy.getCapabilities().isMultiBufferAvailable());
		Config.stdout.println("VS:" + SwingUtilities3.isVsyncRequested(swingWindow));
		//this code sets to full screen
		GraphicsDevice device = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices()[0];
		device.setFullScreenWindow(swingWindow);
		//add esc key for exit
		swingWindow.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					Util.shutdown();
				}
			}
		});
		//set up mouse control
		try {
			mouse = new Robot();
		} catch (AWTException e) {
			e.printStackTrace(Config.stderr);
		}
		int[] pixels = new int[16 * 16];
		Image image = Toolkit.getDefaultToolkit().createImage(
		        new MemoryImageSource(16, 16, pixels, 0, 16));
		Cursor transparentCursor =
		        Toolkit.getDefaultToolkit().createCustomCursor
		             (image, new Point(0, 0), "invisibleCursor");
		swingWindow.getContentPane().setCursor(transparentCursor);
		
		//other core elements (these start their own threads).
		videoReceiver = new FrameReceiver(this);
		cameraSender = new CameraSender(Config.CAMERA_STREAM_START_PORT + getID());
		//set the ping and control sockets to listen
		try {
			pingSocket = new MulticastSocket(Config.MULTICAST_PING_PORT);
			pingSocket.joinGroup(InetAddress.getByName(Config.MULTICAST_ADDRESS));
			controlSocket = new MulticastSocket(Config.MULTICAST_CONTROL_PORT_CPP);
			controlSocket.joinGroup(InetAddress.getByName(Config.MULTICAST_ADDRESS));
		} catch (IOException e) {
			e.printStackTrace();
		}
		//set up a thread to check the pings and commands
		Config.stdout.println("Setting up ping listener.");
		new Thread() {
			public void run() {
				while(true) {
					checkPing();
					checkCommand();
				}
			}
		}.start();
	}
	
	boolean mmove = false;
	
	public void checkPing() {
		DatagramPacket p = new DatagramPacket(pingMsg, pingMsg.length);
		try {
			pingSocket.receive(p);
			long time = Util.bytes2Long(p.getData());
			timeOffsetOfServer = time - System.currentTimeMillis();
			if(verbose) Config.stdout.println("Received ping from server. Server time is " + time);
			//also in this thread make a mouse click to keep screen awake
			mouse.mousePress(InputEvent.BUTTON1_MASK);
            mouse.mouseRelease(InputEvent.BUTTON1_MASK);
            if(mmove) {
            	mouse.mouseMove(30, 30);
            } else {
            	mouse.mouseMove(31, 30);
            }
            mmove = !mmove;
		} catch (IOException e) {
			e.printStackTrace(Config.stderr);
		}
	}
	
	public void checkCommand() {
		DatagramPacket p = new DatagramPacket(commandMsg, commandMsg.length);
		try {
			controlSocket.receive(p);
			String msg = Util.bytes2String(p.getData(), p.getLength());
			statusText = "Received control message: " + msg + " (length = " + p.getLength() + ")";
			if(verbose) Config.stdout.println(statusText);
			//now we have a command, interpret
			if(msg.startsWith("time")) {
				String[] components = msg.split("[ ]");
				int delayFrames = Integer.parseInt(components[0].trim());
				videoReceiver.delaySeconds = delayFrames / Config.FPS;
			} else if(msg.equals("stopcamera")) {
				cameraSender.stopCameraVideoStream();
			} else if(msg.equals("startcamera")) {
				cameraSender.startCameraVideoStream();
			} else if(msg.equals("reboot")) {
				//TODO
			} else if(msg.equals("restartapp")) {
				//TODO
			}
		} catch (IOException e) {
			e.printStackTrace(Config.stderr);
		}
	}

	public void drawFullScreen(BufferedImage nextImg) {
		if(USE_GL) {
			glRenderer.SetNextImage(nextImg);
		} else {
			if(verbose) Config.stdout.println("Calling drawFullSCreen in non-GL mode");
			mainPanel.addFrame(nextImg);
			mainPanel.invalidate();				
			swingWindow.invalidate();
			swingWindow.repaint();
			//swingWindow.getGraphics().drawImage(nextImg, 0, 0, Config.SCREEN_WIDTH, Config.SCREEN_HEIGHT, null);
			//swingWindow.getGraphics().drawString(statusText, 100, 100);
		}
	}
	
	public long getServerTime() {
		return System.currentTimeMillis() + timeOffsetOfServer;
	}
	
	public int getID() {
		return id;
	}
	
	public class MainPanel extends JPanel {
		
		private static final long serialVersionUID = 1L;
		
		
		BufferedImage[] buffer = new BufferedImage[(int) (Config.FPS * 10)];
		private int m_BufferWritePos = 0;
		private int m_BufferReadPos = 0;
		
		public void addFrame(BufferedImage newImage) {
			buffer[m_BufferWritePos] = newImage;
			m_BufferWritePos++;
			if(m_BufferWritePos >= buffer.length) {
				m_BufferWritePos = 0;
			}
		}
		
		@Override
		protected void paintComponent(Graphics g) {
			if(verbose) Config.stdout.println("Swing redraw");
//			super.paintComponent(g);
			m_BufferReadPos = (m_BufferWritePos + buffer.length - 1) % buffer.length;
			if(verbose) Config.stdout.println("Paint!! write:" + m_BufferWritePos + ", read:" + m_BufferReadPos);
			g.drawImage(buffer[m_BufferReadPos], 0, 0, Config.SCREEN_WIDTH, Config.SCREEN_HEIGHT, null);
			m_BufferReadPos++;
			if(m_BufferReadPos >= buffer.length) {
				m_BufferReadPos = 0;
			}
			//g.drawString(statusText, 100, 100);
		}
	}
}
