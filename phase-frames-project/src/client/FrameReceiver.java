package client;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.HashMap;

import javax.imageio.ImageIO;

import common.Config;
import common.UDPPacketToFrameBuilder;
import common.Util;

public class FrameReceiver {
	
	//
	public static final boolean verbose = true;

	//owner
	final ClientMain clientMain;
	
	//the multicast socket for listening for video packets
	MulticastSocket ms;
	// A byte array to read into (max size of 65536, could be smaller)
	byte[] buffer = new byte[65536];
	
	//data structure to store UDP buffers whilst building picture
	HashMap<Integer, UDPPacketToFrameBuilder> frameBuffers = new HashMap<Integer, UDPPacketToFrameBuilder>();

	//ring buffer settings
	double delaySeconds = 0; // delay * fps must not be more than ringBufferSize.
	int ringBufferSize = Config.DEVICE_BUFFER_SIZE_FRAMES; // 250 frames at 25fps = 10s.
	BufferedImage[] ringBuffer = new BufferedImage[ringBufferSize];
	int ringBufferIndex = 0;
	
	//recycleable storage (may not be serving any purpose!)
	byte[] numberBuffer = new byte[4];
	byte[] tempFrameBuffer = new byte[1000000];
	
	public FrameReceiver(ClientMain _clientMain) {
		this.clientMain = _clientMain;
		try {
			ms = new MulticastSocket(Config.MULTICAST_STREAM_PORT);
			ms.joinGroup(InetAddress.getByName(Config.MULTICAST_ADDRESS));
		} catch (IOException e) {
			e.printStackTrace();
		}
		//set up own thread reading from the stream
		new Thread() {
			public void run() {
				while (true) {
					checkForImage();	//is this blocking??
				}
			}
		}.start();
	}
	
	void checkForImage() {
		if(verbose) Config.stdout.println("Waiting for datagram... ");
		DatagramPacket p = new DatagramPacket(buffer, buffer.length);
		try {
			ms.receive(p);
		} catch (IOException e) {
			e.printStackTrace();
		}
		byte[] data = p.getData().clone(); // any point to recycle this?
		// /////
		// begin new byte code
		// Read incoming data into a ByteArrayInputStream
		for (int i = 0; i < 4; i++) {
			numberBuffer[i] = data[i];
		}
		int frameNum = Util.bytes2Int(numberBuffer);
		for (int i = 0; i < 4; i++) {
			numberBuffer[i] = data[i + 4];
		}
		int packetNum = Util.bytes2Int(numberBuffer);
		for (int i = 0; i < 4; i++) {
			numberBuffer[i] = data[i + 8];
		}
		int totalPackets = Util.bytes2Int(numberBuffer);
		if (frameBuffers.containsKey(frameNum)) {
			frameBuffers.get(frameNum).AddPacket(data, packetNum, p.getLength());
		} else {
			UDPPacketToFrameBuilder newBuffer = new UDPPacketToFrameBuilder(frameNum, totalPackets);
			newBuffer.AddPacket(data, packetNum, p.getLength());
			frameBuffers.put(frameNum, newBuffer);
		}
		if (!frameBuffers.get(frameNum).IsBufferReady()) {
			return;
		}
		byte[] imgData = frameBuffers.get(frameNum).ConstructFrame(); // recycle this?
		frameBuffers.remove(frameNum);
		// /////////////////////
		// end receiver code (this may have exited if we don't have a complete
		// image).
		// if we are still here then imgData is full.
		// new image has arrived: do decompression in a new thread.
		new Thread() {
			public void run() {
				if(verbose) Config.stdout.println("Received datagram with " + data.length
						+ " bytes.");
				// Read incoming data into a ByteArrayInputStream
				ByteArrayInputStream bais = new ByteArrayInputStream(imgData);
				// start timing here - we want to know how long the decompress
				// and
				// write
				// takes, not the read from buffer
				long time = System.currentTimeMillis();
				// We need to unpack JPG and put it in the PImage video
				// Make a BufferedImage out of the incoming bytes

				BufferedImage img = null;
				try {
					img = ImageIO.read(bais);
				} catch (Exception e) {
					e.printStackTrace(Config.stderr);
				}
				if(verbose) Config.stdout.println("Time to decompress: "
						+ (System.currentTimeMillis() - time) + "ms");
				time = System.currentTimeMillis();

				// add the new img to the ring buffer
				ringBuffer[ringBufferIndex] = img;
				int frameDelay = (int) (delaySeconds * Config.FPS);
				int nextImageIndex = (ringBufferIndex - frameDelay + ringBufferSize)
						% ringBufferSize;
				BufferedImage nextImg = ringBuffer[nextImageIndex];
				new Thread() {
					public void run() {
						long drawTime = System.currentTimeMillis();
						clientMain.drawFullScreen(nextImg);
						if(verbose) Config.stdout.println("Time to draw : "
								+ (System.currentTimeMillis() - drawTime)
								+ "ms");

					}
				}.start();
				// swingWindow.getGraphics().drawImage(nextImg, 0, 0, W, H,
				// null);

				// stdout.println("Tried to draw : " + nextImageIndex +
				// " : buffer went into :" + ringBufferIndex);
				// step through the ring buffer
				ringBufferIndex = (ringBufferIndex + 1) % ringBufferSize;

				if(verbose) Config.stdout.println("Time to draw : "
						+ (System.currentTimeMillis() - time) + "ms");
				time = System.currentTimeMillis();
			}
		}.start();
	}
}
