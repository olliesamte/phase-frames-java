package client;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.MulticastSocket;

import com.sun.javafx.css.parser.StopConverter;

import common.Config;
import common.Util;

public class CameraSender {
	
	//
	public static final boolean verbose = true;

	//the port to send on
	int port;

	public CameraSender(int _port) {
		//work out the port to send on
		this.port = _port;
		startCameraVideoStream();
	}
	
	public void stopCameraVideoStream() {
		try {
			Runtime.getRuntime().exec(new String[] {"/usr/bin/killall", "raspivid"});
			Runtime.getRuntime().exec(new String[] {"/usr/bin/killall", "vlc"});
		} catch (IOException e) {
			e.printStackTrace(Config.stderr);
		}
	}

	public void startCameraVideoStream() {
		//always stop first
		//stopCameraVideoStream();
		Config.stdout.println("Trying startcamera command");
		//now safe to run
		String scriptpath = "/home/pi/git/phase-frames-java/scripts/runvideo.sh";
		String[] commandarray = new String[] {scriptpath, ""+port, ""+Config.MOVIE_WIDTH, ""+Config.MOVIE_HEIGHT, ""+(int)Config.FPS};
		try {
			Runtime.getRuntime().exec(commandarray);
		} catch (IOException e) {
			e.printStackTrace(Config.stderr);
		}
		Config.stdout.println("Starting writing camera video stream with command: " + commandarray[0] + ", on port " + port);
	}
	
	public static void main(String[] args) throws Exception {
		CameraSender s = new CameraSender(Config.CAMERA_STREAM_START_PORT + Util.findMyIndex());
		//listen for messages
		//the multicast socket for listening for timing pings
		MulticastSocket controlSocket = new MulticastSocket(Config.MULTICAST_CONTROL_PORT_JAVA);
			String hostname = Util.findMyHostname();
			int id = Util.findMyIndex();
		byte[] commandMsg = new byte[4000]; //arbitrary length!
		//stay alive
		new Thread() {
			public void run() {
				while(true) {
					try {
						DatagramPacket p = new DatagramPacket(commandMsg, commandMsg.length);
						controlSocket.receive(p);
						String msg = Util.bytes2String(p.getData(), p.getLength());
						if(msg.equals("r " + id)) {
							//reset?
							s.stopCameraVideoStream();
							Thread.sleep(2000);
							s.startCameraVideoStream();
						}
						Thread.sleep(500);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			}
		}.start();
	}

}
