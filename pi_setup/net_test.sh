#!/bin/bash

# Keep checking network

errcount=0

while true
do
	ping -q -w 1 -c 1 `ip r | grep default | cut -d ' ' -f 3` > /dev/null && status="ok" || status="error"
	# If connection fails, reboot
	echo "Status is $status"
	if [ $status == "ok" ]; 
	then
		errcount=0
		echo "Network is on."
	else 
		errcount=$[errcount + 1]
		echo "Network is off. Errors: $errcount"
		if [ $errcount -gt 3 ];
		then 
			echo "Network errcount > 3. Rebooting."
			sudo reboot
		fi
	fi
	sleep 4
done

