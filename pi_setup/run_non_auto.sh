#!/bin/bash

#Do your thing here.

SCRIPTDIR=`dirname $0`
echo "This is the script $0."

# run the simplified java program that simply starts the video
# note we can probably get rid of most of the classpath entries here
/usr/bin/java -Djava.library.path=/usr/lib/jni -cp /usr/share/java/lwjgl.jar:/usr/share/java/lwjgl_test.jar:/usr/share/java/lwjgl_util.jar:/home/pi/git/phase-frames-java/phase-frames-project/bin -Xmx768M client.CameraSender &

# run the open frameworks program
cd /home/pi/of/examples/graphics/imageSequenceExample
/usr/bin/make run