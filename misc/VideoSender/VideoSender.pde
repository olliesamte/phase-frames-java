import processing.video.*;

import java.net.*;
import java.io.*;
import java.nio.*;
import javax.imageio.*;
import java.awt.image.*; 
import java.util.*;
import javax.imageio.stream.*;

// This is the port we are sending to
int clientPort = 9100; 
// This is our object that sends UDP out
DatagramSocket ds; 
// Capture object
Capture cam;

String receiverIP = "224.1.1.1";      //multicast address
//String receiverIP = "192.168.0.5";      //unicast address

int frameNumber = 0;

long lastCaptureTime = 0;
long lastDrawTime = 0;

void setup() {
  String[] cameras = Capture.list();
  
  if (cameras.length == 0) {
    println("There are no cameras available for capture.");
    exit();
  } else {
    println("Available cameras:");
    for (int i = 0; i < cameras.length; i++) {
      println(cameras[i]);
    }
  }
  
  checkFormats();
  
  size(1280,720);
  // Setting up the DatagramSocket, requires try/catch
  try {
    ds = new DatagramSocket();
//    ds.setTimeToLive(1);
  } catch (SocketException e) {
    e.printStackTrace();
  }
  // Initialize Camera
  cam = new Capture(this, cameras[1]);
  cam.start();
}

void captureEvent( Capture c ) {
  println("Capture Timer : " + (System.currentTimeMillis() - lastCaptureTime));
  lastCaptureTime = System.currentTimeMillis();
  c.read();
  // Whenever we get a new image, send it!
  broadcast(c, frameNumber++);
}

void draw() {
  println("Draw Timer : " + (System.currentTimeMillis() - lastDrawTime));
  lastDrawTime = System.currentTimeMillis();
  image(cam,0,0);
}


// Function to broadcast a PImage over UDP
// Special thanks to: http://ubaa.net/shared/processing/udp/
// (This example doesn't use the library, but you can!)
void broadcast(PImage img, int frame) {

  // We need a buffered image to do the JPG encoding
  BufferedImage bimg = new BufferedImage( img.width,img.height, BufferedImage.TYPE_BYTE_GRAY );

  // Transfer pixels from localFrame to the BufferedImage
  img.loadPixels();
  bimg.setRGB( 0, 0, img.width, img.height, img.pixels, 0, img.width);

  // Need these output streams to get image as bytes for UDP communication
  ByteArrayOutputStream baStream	= new ByteArrayOutputStream();
  BufferedOutputStream bos		= new BufferedOutputStream(baStream);

  // Turn the BufferedImage into a JPG and put it in the BufferedOutputStream
  // Requires try/catch
  long time = System.currentTimeMillis();
  try {
    ImageOutputStream ios = ImageIO.createImageOutputStream(bos);
    Iterator<ImageWriter> iter = ImageIO.getImageWritersByFormatName("jpeg");
    ImageWriter writer = iter.next();
    ImageWriteParam iwp = writer.getDefaultWriteParam();
    iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
    iwp.setCompressionQuality(1f);
    writer.setOutput(ios);
    writer.write(null, new IIOImage(bimg, null, null), iwp);
    writer.dispose();
    bos.flush();
    //ImageIO.write(bimg, "jpg", bos);
  } 
  catch (IOException e) {
    e.printStackTrace();
  }
  println("Compress time : " + (System.currentTimeMillis() - time));

  time = System.currentTimeMillis();
  // Get the byte array, which we will send out via UDP!
 
  byte[] fullData = baStream.toByteArray();
  println("Stream Copy Time : " + (System.currentTimeMillis() - time));
  // Split into chunks
  int total = fullData.length;
  int extraDataSize = 12;
  int maxSize = 65000 - extraDataSize;      
//  int maxSize = 30000 - extraDataSize;      //ollie changing this to smaller size for multicast
//  int maxSize = 200 - extraDataSize;
  int current = 0;
  boolean done = false;
  byte[] frameNumberBytes = my_int_to_bb_le(frame);
  int totalPackets = (int) Math.ceil((double)fullData.length / maxSize);
  byte[] totalPacketsBytes = my_int_to_bb_le(totalPackets);
  
  println("Sending frame : " + frame + " in : " + totalPackets + " packets, total size : " + total);
  time = System.currentTimeMillis();
  for(int i = 0; i < totalPackets; i++) {
    int diff = total - current;
    // we add 3 values at the start for frame number, part number and total parts for this frame
    // int for frame numer, int for part number and int for total parts.
    // int is 4 bytes (32 bit), 
    // so first 12 bytes are our data.
    
    int newBufferSize = (diff < maxSize ? diff : maxSize) + extraDataSize;
    
    byte[] packet = new byte[newBufferSize];
    for(int x = 0; x < frameNumberBytes.length; x++) {
      packet[x] = frameNumberBytes[x];
    }
    
    byte[] packetNumberBytes = my_int_to_bb_le(i);
    for(int x = 0; x < packetNumberBytes.length; x++) {
      packet[x + 4] = packetNumberBytes[x]; 
    }
    
    for(int x = 0; x < totalPacketsBytes.length; x++) {
      packet[x + 8] = totalPacketsBytes[x];
    }
    
    for(int x = 0; x < newBufferSize - extraDataSize; x++) {
      
      packet[x + extraDataSize] = fullData[(i * maxSize) + x];
    }
    // Send PNG data as a datagram
    
    //println("Sending datagram with " + packet.length + " bytes : " + frame + " : " + i + " : " + totalPackets);
    try {
      //bogus data test
//      byte[] bogus = new byte[2000];
//      packet = bogus;
      ds.send(new DatagramPacket(packet,packet.length, InetAddress.getByName(receiverIP),clientPort));
    } 
    catch (Exception e) {
      e.printStackTrace();
      exit();
    }
    current += newBufferSize - extraDataSize;
  }
  println("Send timer : " + (System.currentTimeMillis() - time));
}

public static  byte[] my_int_to_bb_le(int myInteger){
    return ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(myInteger).array();
}

public static int my_bb_to_int_le(byte [] byteBarray){
    return ByteBuffer.wrap(byteBarray).order(ByteOrder.LITTLE_ENDIAN).getInt();
}

public static  byte[] my_int_to_bb_be(int myInteger){
    return ByteBuffer.allocate(4).order(ByteOrder.BIG_ENDIAN).putInt(myInteger).array();
}

public static int my_bb_to_int_be(byte [] byteBarray){
    return ByteBuffer.wrap(byteBarray).order(ByteOrder.BIG_ENDIAN).getInt();
}

void checkFormats() {
   HashSet set = new HashSet();
    // Get list of all informal format names understood by the current set of registered readers
    String[] formatNames = ImageIO.getReaderFormatNames();
    for (int i = 0; i < formatNames.length; i++) {
      set.add(formatNames[i].toLowerCase());
    }
    System.out.println("Supported read formats: " + set);
    set.clear();
    // Get list of all informal format names understood by the current set of registered writers
    formatNames = ImageIO.getWriterFormatNames();
    for (int i = 0; i < formatNames.length; i++) {
      set.add(formatNames[i].toLowerCase());
    }
    System.out.println("Supported write formats: " + set);
    set.clear();
    // Get list of all MIME types understood by the current set of registered readers
    formatNames = ImageIO.getReaderMIMETypes();
    for (int i = 0; i < formatNames.length; i++) {
      set.add(formatNames[i].toLowerCase());
    }
    System.out.println("Supported read MIME types: " + set);
    set.clear();
    // Get list of all MIME types understood by the current set of registered writers
    formatNames = ImageIO.getWriterMIMETypes();
    for (int i = 0; i < formatNames.length; i++) {
      set.add(formatNames[i].toLowerCase());
    }
    System.out.println("Supported write MIME types: " + set); 
}

