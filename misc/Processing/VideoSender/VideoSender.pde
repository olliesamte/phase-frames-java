import processing.video.*;

import java.util.*;
import java.net.*;
import java.io.*;
import javax.imageio.*;
import java.awt.image.*; 

boolean SEND_UNCOMPRESSED = false;
boolean GRAYSCALE = true;

int imageWidth = 640;
int imageHeight = 360;

//String receiverIP = "10.65.63.23";
String receiverIP = "224.1.1.1";      //multicast address

// This is the port we are sending to
int clientPort = 9100; 
// This is our object that sends UDP out
DatagramSocket ds; 
// Capture object
Capture cam;

int[] noise;

void setup() {
  
    //Testing purposes - fill the image with noise first
  noise = new int[imageWidth * imageHeight];
  for(int i = 0; i < imageWidth; i++) {
    for(int j = 0; j < imageHeight; j++) {
      noise[i + j*imageWidth] = (int)random(255);   
    }
  }
  
  checkFormats();
  String[] cameras = Capture.list();
  
  if (cameras.length == 0) {
    println("There are no cameras available for capture.");
    exit();
  } else {
    println("Available cameras:");
    for (int i = 0; i < cameras.length; i++) {
      println(cameras[i]);
    }
  }
  
  size(imageWidth,imageHeight);
  // Setting up the DatagramSocket, requires try/catch
  try {
    ds = new DatagramSocket();
  } catch (SocketException e) {
    e.printStackTrace();
  }
  // Initialize Camera
//  cam = new Capture(this, cameras[3]);
  cam = new Capture(this, cameras[3]);
  cam.start();
}

int count = 0;

void captureEvent( Capture c ) {
  c.read();
  // Whenever we get a new image, send it!
  //simple halving of camera framerate
  if(count++ % 2 == 0) {
    broadcast(c);
  }
}

void draw() {
  image(cam,0,0);
}

// Function to broadcast a PImage over UDP
// Special thanks to: http://ubaa.net/shared/processing/udp/
// (This example doesn't use the library, but you can!)
void broadcast(PImage img) {
    println("Camera image is: " + img.width + "x" + img.height);
  // We need a buffered image to do the JPG encoding
  BufferedImage bimg = null;
  if(GRAYSCALE) {
    bimg = new BufferedImage( imageWidth,imageHeight, BufferedImage.TYPE_BYTE_GRAY );
  } else {
    bimg = new BufferedImage( imageWidth,imageHeight, BufferedImage.TYPE_INT_RGB );
  }
  // Transfer pixels from localFrame to the BufferedImage
  img.loadPixels();
   //Ollie mod TODO -- also need this line to be greyscale... not so easy
   //see here for getting into the low-level: basically accessing raw pixels in the right format.
   //    --- http://www.coderanch.com/t/402473/java/java/changing-gray-pixels
   //the below method assumes RGB so is wasting resources. 3 x the data.
  
  //test code: filling the BufferedImage with noise so that we're sending a true image
  bimg.setRGB( 0, 0, imageWidth, imageHeight, noise, 0, imageWidth);
  
  bimg.setRGB( 0, 0, img.width, img.height, img.pixels, 0, img.width);
  
  // Need these output streams to get image as bytes for UDP communication
  ByteArrayOutputStream baStream	= new ByteArrayOutputStream();
  BufferedOutputStream bos		= new BufferedOutputStream(baStream);
  // Turn the BufferedImage into a JPG and put it in the BufferedOutputStream
  // Requires try/catch
  try {
    if(SEND_UNCOMPRESSED) {
       ImageIO.write(bimg, "bmp", bos);
    } else {
      ImageIO.write(bimg, "jpg", bos);
    }
  } 
  catch (IOException e) {
    e.printStackTrace();
  }
  // Get the byte array, which we will send out via UDP!
  byte[] packet = baStream.toByteArray();
  // Send JPEG data as a datagram
//  println("Sending datagram with " + packet.length + " bytes");
  try {
    ds.send(new DatagramPacket(packet,packet.length, InetAddress.getByName(receiverIP),clientPort));
  } 
  catch (Exception e) {
    e.printStackTrace();
  }
}


void checkFormats() {
   HashSet set = new HashSet();
    // Get list of all informal format names understood by the current set of registered readers
    String[] formatNames = ImageIO.getReaderFormatNames();
    for (int i = 0; i < formatNames.length; i++) {
      set.add(formatNames[i].toLowerCase());
    }
    System.out.println("Supported read formats: " + set);
    set.clear();
    // Get list of all informal format names understood by the current set of registered writers
    formatNames = ImageIO.getWriterFormatNames();
    for (int i = 0; i < formatNames.length; i++) {
      set.add(formatNames[i].toLowerCase());
    }
    System.out.println("Supported write formats: " + set);
    set.clear();
    // Get list of all MIME types understood by the current set of registered readers
    formatNames = ImageIO.getReaderMIMETypes();
    for (int i = 0; i < formatNames.length; i++) {
      set.add(formatNames[i].toLowerCase());
    }
    System.out.println("Supported read MIME types: " + set);
    set.clear();
    // Get list of all MIME types understood by the current set of registered writers
    formatNames = ImageIO.getWriterMIMETypes();
    for (int i = 0; i < formatNames.length; i++) {
      set.add(formatNames[i].toLowerCase());
    }
    System.out.println("Supported write MIME types: " + set); 
}
