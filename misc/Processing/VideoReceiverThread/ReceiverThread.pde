// Daniel Shiffman
// <http://www.shiffman.net>

// A Thread using receiving UDP

import java.net.*;
import java.io.*;
import java.nio.*;

class ReceiverThread extends Thread {

  // Port we are receiving.
  int port = 9100; 
  DatagramSocket ds; 
  // A byte array to read into (max size of 65536, could be smaller)
  byte[] buffer = new byte[65536]; 
  ArrayList<byte[]> frameList1 = new ArrayList<byte[]>();
  int buffer1FrameNum = -1;
  int buffer1TargetPackets = -1;
  int finalBuffer1Length = -1;
  ArrayList<byte[]> frameList2 = new ArrayList<byte[]>();
  int buffer2FrameNum = -1;
  int buffer2TargetPackets = -1;
  int finalBuffer2Length = -1;
  
  int extraDataSize = 12;
  int maxSize = 60000 - extraDataSize;

  boolean running;    // Is the thread running?  Yes or no?
  boolean available;  // Are there new tweets available?

  // Start with something 
  PImage img;

  ReceiverThread (int w, int h) {
    img = createImage(w,h,RGB);
    running = false;
    available = true; // We start with "loading . . " being available

    try {
      ds = new DatagramSocket(port);
    } catch (SocketException e) {
      e.printStackTrace();
    }
  }

  PImage getImage() {
    
    available = false;
    return img;
  }

  boolean available() {
    return available;
  }

  // Overriding "start()"
  void start () {
    running = true;
    super.start();
  }

  // We must implement run, this gets triggered by start()
  void run () {
    while (running) {
      checkForImage();
      // New data is available!
      //available = true;
    }
  }
  
  void clearBuffer1() {
    // Clear buffer 1
    frameList1.clear();
    buffer1FrameNum = -1;
    buffer1TargetPackets = -1;
    finalBuffer1Length = -1;
  }
  
  void clearBuffer2() {
    // Clear buffer 2
    frameList2.clear();
    buffer2FrameNum = -1;
    buffer2TargetPackets = -1;
    finalBuffer2Length = -1;
  }

  void checkForImage() {
    DatagramPacket p = new DatagramPacket(buffer, buffer.length);
    try {
      ds.receive(p);
    } 
    catch (IOException e) {
      e.printStackTrace();
    } 
    
    byte[] data = p.getData().clone();

    println("Received datagram with " + p.getLength() + " bytes." );
    int packetLength = p.getLength() - extraDataSize;
    
    byte[] numberBuffer = new byte[4];

    // Read incoming data into a ByteArrayInputStream
    for(int i = 0; i < 4; i++) {
      numberBuffer[i] = data[i];
    }
    int frameNum = my_bb_to_int_le(numberBuffer);
    for(int i = 0; i < 4; i++) {
      numberBuffer[i] = data[i + 4];
    }
    int packetNum = my_bb_to_int_le(numberBuffer);
    for(int i = 0; i < 4; i++) {
      numberBuffer[i] = data[i + 8];
    }
    int totalPackets = my_bb_to_int_le(numberBuffer);
    
    if(buffer1FrameNum != frameNum && buffer2FrameNum != frameNum) {
      // We aren't buffering this frame yet.
      if(buffer1FrameNum == -1) {
        // Buffer 1 is empty
        println("Starting buffer 1 with frame : " + frameNum);
        buffer1FrameNum = frameNum;
        buffer1TargetPackets = totalPackets;
        finalBuffer1Length = 0;
      } else if(buffer2FrameNum == -1) {
        // Buffer 2 is empty
        println("Starting buffer 2 with frame : " + frameNum);
        buffer2FrameNum = frameNum;
        buffer2TargetPackets = totalPackets;
        finalBuffer2Length = 0;
      } else {
        println("ERROR : no available buffer!");
        // Clear the older buffer
        if(buffer1FrameNum < buffer2FrameNum) {
          clearBuffer1();
          buffer1FrameNum = frameNum;
          buffer1TargetPackets = totalPackets;
          finalBuffer1Length = 0;
        } else {
          clearBuffer2();
          buffer2FrameNum = frameNum;
          buffer2TargetPackets = totalPackets;
          finalBuffer2Length = 0;
        }
        //return;
      }
    }
    
    boolean finished = false;
    ArrayList<byte[]> listBuffer = null;
    int finalBufferLength = 0;
    if(buffer1FrameNum == frameNum) {
      frameList1.add(data);
      println("Added to 1! : " + (packetLength) );
      finalBuffer1Length += packetLength;
      if(frameList1.size() == buffer1TargetPackets) {
        finished = true;
        listBuffer = frameList1;
        finalBufferLength = finalBuffer1Length;
      }
    } else if(buffer2FrameNum == frameNum) {
      frameList2.add(data);
      println("Added to 2! : " + (packetLength) );
      finalBuffer2Length += packetLength;
      if(frameList2.size() == buffer2TargetPackets) {
        finished = true;
        listBuffer = frameList2;
        finalBufferLength = finalBuffer2Length;
      }
    }
    
    println("BUffer Sizes : " + frameList1.size() + " : " + frameList2.size() + " : " + finished);
    
    // If the list length matches the expected count, convert to image
    if(finished) {
      println("1");
      byte[] imgData = new byte[finalBufferLength];
      
      for(int i = 0; i < listBuffer.size(); i++) {
        println("2 : " + i);
        byte[] temp = listBuffer.get(i);
        
        println("3");
        byte[] newNumberBuffer = new byte[4];
        for(int x = 0; x < 4; x++) {
          newNumberBuffer[x] = temp[x];
        }
        int loopFrameNum = my_bb_to_int_le(newNumberBuffer);
        for(int x = 0; x < 4; x++) {
          newNumberBuffer[x] = temp[x + 4];
        }
        int loopPacketNum = my_bb_to_int_le(newNumberBuffer);
        for(int x = 0; x < 4; x++) {
          newNumberBuffer[x] = temp[x + 8];
        }
        int loopTotalPacketNum = my_bb_to_int_le(newNumberBuffer);
        println("4");
        
        println("5 : " + loopFrameNum + " : " + loopPacketNum + " : " + loopTotalPacketNum);
        byte[] imagePacket = null;
        int packetSize = -1;
        if(loopPacketNum < listBuffer.size() -1) {
          imagePacket = new byte[maxSize];
          packetSize = maxSize;
        } else {
          // Last packet
          packetSize = finalBufferLength % maxSize;
          imagePacket = new byte[packetSize];
        }
        println("Copying packet " + loopPacketNum + " of size : " + packetSize);
        for(int y = 0; y < packetSize; y++) {
          imgData[loopPacketNum * maxSize + y] = temp[y + extraDataSize];
        }
      }
      println("6");
      ByteArrayInputStream bais = new ByteArrayInputStream( imgData );
      println("7");
      // We need to unpack JPG and put it in the PImage img
      img.loadPixels();
      try {
        // Make a BufferedImage out of the incoming bytes
        println("8");
        BufferedImage bimg = ImageIO.read(bais);
        // Put the pixels into the PImage
        println("9 : " + img.width + " : " + img.height + " : " + img.pixels.length);
        bimg.getRGB(0, 0, img.width, img.height, img.pixels, 0, img.width);
        println("10");
      } 
      catch (Exception e) {
        e.printStackTrace();
      }
      println("11");
      // Update the PImage pixels
      img.updatePixels();
      available = true;
      
      // Clear buffer
      if(buffer1FrameNum == frameNum) {
        clearBuffer1();
      } else {
        clearBuffer2();
      }
    }
  }


  // Our method that quits the thread
  void quit() {
    System.out.println("Quitting."); 
    running = false;  // Setting running to false ends the loop in run()
    // In case the thread is waiting. . .
    interrupt();
  }
  
  public  byte[] my_int_to_bb_le(int myInteger){
    return ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(myInteger).array();
  }

  public int my_bb_to_int_le(byte [] byteBarray){
    return ByteBuffer.wrap(byteBarray).order(ByteOrder.LITTLE_ENDIAN).getInt();
  }

  public  byte[] my_int_to_bb_be(int myInteger){
    return ByteBuffer.allocate(4).order(ByteOrder.BIG_ENDIAN).putInt(myInteger).array();
  }

  public int my_bb_to_int_be(byte [] byteBarray){
    return ByteBuffer.wrap(byteBarray).order(ByteOrder.BIG_ENDIAN).getInt();
  }
}

