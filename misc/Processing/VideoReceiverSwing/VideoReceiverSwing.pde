import java.net.*;
import java.io.*;

import java.awt.image.*; 
import javax.imageio.*;
import javax.swing.*;

boolean DRAW_SWING = true;

// Port we are receiving.
int port = 9100; 
DatagramSocket ds; 
// A byte array to read into (max size of 65536, could be smaller)
byte[] buffer = new byte[65536]; 
JFrame swingWindow;

float delayMS = 1000;
int ringBufferSize = 250;  //250 frames at 25fps = 10s.
BufferedImage[] ringBuffer = new BufferedImage[ringBufferSize];
int ringBufferIndex = 0;

PImage video;

void setup() {
  size(640, 360);
  try {
    ds = new DatagramSocket(port);
  } 
  catch (SocketException e) {
    e.printStackTrace();
  } 
  video = createImage(640, 360, RGB);
  //here is the Swing window...
  swingWindow = new JFrame("LOOK AT ME NOT HIM!");
  swingWindow.setSize(1920, 1080);
  swingWindow.setVisible(true);
}

void draw() {
  // checkForImage() is blocking, stay tuned for threaded example!
  checkForImage();
  // Draw the image
  if(!DRAW_SWING) {
    background(0);
    imageMode(CENTER);
    image(video, width/2, height/2);
  }
}

void checkForImage() {
  DatagramPacket p = new DatagramPacket(buffer, buffer.length); 
  try {
    ds.receive(p);
  } 
  catch (IOException e) {
    e.printStackTrace();
  } 
  byte[] data = p.getData();
  println("Received datagram with " + data.length + " bytes." );
  // Read incoming data into a ByteArrayInputStream
  ByteArrayInputStream bais = new ByteArrayInputStream( data );
  //start timing here - we want to know how long the decompress and write takes, not the read from buffer
  long time = System.currentTimeMillis ();
  // We need to unpack JPG and put it in the PImage video
  // Make a BufferedImage out of the incoming bytes
  BufferedImage img = null;
  try {
    img = ImageIO.read(bais);
  } 
  catch (Exception e) {
    e.printStackTrace();
  }
  System.out.println("Time to decompress: " + (System.currentTimeMillis() - time) + "ms"); 
  time = System.currentTimeMillis();
  if (!DRAW_SWING) {
    video.loadPixels();
    try { 
      // Put the pixels into the video PImage
      img.getRGB(0, 0, video.width, video.height, video.pixels, 0, video.width);
    } 
    catch (Exception e) {
      e.printStackTrace();
    }
    // Update the PImage pixels
    video.updatePixels();
  } else {
    
    //ok so add the new img to the ring buffer
    //TODO
    
    swingWindow.getGraphics().drawImage(img, 0, 0, null);
  }
  System.out.println("Time to draw: " + (System.currentTimeMillis() - time) + "ms");
  time = System.currentTimeMillis();
}

